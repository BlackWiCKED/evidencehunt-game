#include <QObject>

#ifndef EVIDENCETYPE_H
#define EVIDENCETYPE_H

class EvidenceType : public QObject
{
    Q_OBJECT

public:
    EvidenceType() : _id(0), _color("grey"), _colorcode("#ffffff") ,_description(""), _number(0), _image("") {}

    EvidenceType(QString color, QString colorcode, int number, QString image = "") {
        _color = color;
        _colorcode = colorcode;
        _description = "";
        _number = number;
        _image = image;
        emit typeChanged();
    };

    Q_PROPERTY(int id READ id WRITE setId NOTIFY typeChanged)
    int id() { return _id; }

    Q_PROPERTY(QString color READ color NOTIFY typeChanged)
    QString color() { return _color; }

    Q_PROPERTY(QString colorcode READ colorcode NOTIFY typeChanged)
    QString colorcode() { return _colorcode; }

    Q_PROPERTY(QString description READ description NOTIFY typeChanged)
    QString description() { return _description; }

    Q_PROPERTY(int number READ number NOTIFY typeChanged)
    int number() { return _number; }

    Q_PROPERTY(QString image READ image NOTIFY typeChanged)
    QString image() { return _image; }

    void setId(int id) { if(id == _id) return; _id = id; emit typeChanged(); }

    void setDescription(QString description) { _description = description; emit typeChanged(); }

signals:
    void typeChanged();

private:
    int _id;
    QString _color;
    QString _colorcode;
    QString _description;
    int _number;
    QString _image;
};

#endif // EVIDENCETYPE_H
