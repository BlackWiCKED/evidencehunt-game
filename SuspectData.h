#include <QObject>

#ifndef SUSPECTDATA_H
#define SUSPECTDATA_H

class SuspectData : public QObject {

    Q_OBJECT

public:

    SuspectData() : _hasFlag(false), _profile(0), _roleId(0), _flipped(false), _excluded(false), _isKiller(false) {}

    Q_PROPERTY(bool hasFlag READ hasFlag WRITE setHasFlag NOTIFY hasFlagChanged())
    bool hasFlag() const { return _hasFlag; }

    Q_PROPERTY(bool flipped READ flipped NOTIFY flippedChanged())
    bool flipped() const { return _flipped; }

    Q_PROPERTY(bool excluded READ excluded NOTIFY excludedChanged())
    bool excluded() const { return _excluded; }

    Q_PROPERTY(int profile READ profile NOTIFY profileChanged())
    int profile() const { return _profile; }

    Q_PROPERTY(int role READ role NOTIFY roleChanged())
    int role() const { return _roleId; }

    Q_PROPERTY(QString roleText READ roleText NOTIFY roleChanged())
    QString roleText() const { return _role; }

    Q_PROPERTY(bool isKiller READ isKiller/* NOTIFY excludedChanged()*/)
    bool isKiller() const { return _isKiller; }

    Q_PROPERTY(QList<int> evidences READ evidences NOTIFY evidencesChanged())
    QList<int> evidences() const { return _evidences; /*if (_evidences) {return _evidences;} else { QList<int>return QList<int>};*/}

    void setHasFlag(bool flag) {if(flag==_hasFlag) return; _hasFlag = flag; emit hasFlagChanged();}
    void setProfile(int profile) { _profile = profile; emit profileChanged();}
    void setRole(int roleId, QString role) { _roleId = roleId; _role = role; emit roleChanged();}
    void flip() { if (_flipped) return; _flipped = true; emit flippedChanged(); }
    void unflip() { if(!_flipped) return; _flipped = false; emit flippedChanged(); }
    void exclude() { if (_excluded) return; _excluded = true; emit excludedChanged(); }
    void include() { if(!_excluded) return; _excluded = false; emit excludedChanged(); }
    void setIsKiller(bool isKiller) { _isKiller = isKiller; }

    void resetEvidences() {_evidences.clear(); emit evidencesChanged();}
    void assignEvidence(int evidenceId) { _evidences << evidenceId; emit evidencesChanged();  }
    void reloadEvidences() { emit roleChanged(); }

signals:
    void hasFlagChanged();
    void flippedChanged();
    void excludedChanged();
    void profileChanged();
    void roleChanged();
    void evidencesChanged();

private:
    bool _hasFlag;
    int _profile;
    int _roleId;
    QString _role;
    bool _flipped;
    bool _excluded;
    bool _isKiller;

    QList<int> _evidences;

};

#endif // SUSPECTDATA_H
