QT += core gui xml network declarative #phonon

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

symbian:TARGET.CAPABILITY += NetworkServices

SOURCES += main.cpp \
    EvidenceHunt.cpp

exists( ScoreboardCustom.cpp ) {
    SOURCES += ScoreboardCustom.cpp
} else {
    SOURCES += Scoreboard.cpp
}

# Define QMLJSDEBUGGER to enable basic debugging (setting breakpoints etc)
# DEFINES += NO_QMLJSDEBUGGER

HEADERS += \
    SuspectData.h \
    EvidenceType.h \
    TileData.h \
    EvidenceInfo.h \
    EvidenceHuntGame.h \
    Scoreboard.h \
    ScoreboardHandler.h \
    TopPlayerData.h

RESOURCES += \
    EvidenceHunt.qrc

TRANSLATIONS += qml/evidencehunt.en.ts \
               qml/evidencehunt.hu.ts \
               qml/evidencehunt.sr.ts \
               qml/evidencehunt.it.ts

#music_folder.source = music
#music_folder.target =
#DEPLOYMENTFOLDERS = music_folder

include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

symbian {
    DEFINES += Q_EVHUNT_SYMBIAN
} else:win32 {
    DEFINES += Q_EVHUNT_WINDOWS
    RC_FILE += EvidenceHunt.rc
} else:qpa {
    DEFINES += Q_EVHUNT_BLACKBERRY
    CONFIG += device
} else:unix {
    maemo5 {
        DEFINES += Q_EVHUNT_MAEMO
    } else:contains(MEEGO_VERSION_MAJOR,1) {
        DEFINES += Q_EVHUNT_MEEGO
        CONFIG += shareuiinterface-maemo-meegotouch share-ui-plugin share-ui-common mdatauri
        target.path = /opt/evidencehunt/bin
        desktopfile.files = qtc_packaging/debian_harmattan/evidencehunt.desktop
        desktopfile.path = /usr/share/applications
        icon.files = qml/EvidencehuntCore/pics/evidencehunt-icon.png
        icon.path = /usr/share/icons/hicolor/80x80/apps
        splash.files = qml/EvidencehuntCore/pics/evidencehunt-splash.png
        splash.path = /usr/share/$$TARGET
        INSTALLS += \
            target \
            desktopfile \
            icon \
            splash
    } else {
        DEFINES += Q_EVHUNT_LINUX
    }
}

OTHER_FILES += \
    qml/EvidencehuntCore/EvidenceInfo.qml \
    qml/EvidencehuntCore/Explosion.qml \
    qml/EvidencehuntCore/Suspect.qml \
    qml/EvidencehuntCore/Tile.qml \
    qml/EvidencehuntCore/Scoreboard.qml \
    qml/DesktopComponents/Label.qml \
    internationalization/en/startScenes.xml \
    internationalization/en/roleTypes.xml \
    internationalization/en/suspectProperties.xml \
    internationalization/en/suspectNegativeProperties.xml \
    internationalization/hu/startScenes.xml \
    internationalization/hu/roleTypes.xml \
    internationalization/hu/suspectProperties.xml \
    internationalization/hu/suspectNegativeProperties.xml \
    internationalization/sr/startScenes.xml \
    internationalization/sr/roleTypes.xml \
    internationalization/sr/suspectProperties.xml \
    internationalization/sr/suspectNegativeProperties.xml \
    internationalization/it/startScenes.xml \
    internationalization/it/roleTypes.xml \
    internationalization/it/suspectProperties.xml \
    internationalization/it/suspectNegativeProperties.xml \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog \
    qtc_packaging/debian_harmattan/evidencehunt.desktop \
    qml/desktop.qml \
    qml/harmattan.qml \
    qml/blackberry.qml \
    qml/DesktopComponents/Sheet.qml \
    qml/EvidencehuntCore/Welcome.qml \
    qml/EvidencehuntCore/GameMenuBottom.qml \
    qml/EvidencehuntCore/GameMenuTop.qml \
    qml/EvidencehuntCore/Score.qml \
    qml/EvidencehuntCore/SuspectList.qml \
    bar-descriptor.xml \
    qml/EvidencehuntCore/ScoreboardList.qml
