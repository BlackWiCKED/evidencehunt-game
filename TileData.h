#include "EvidenceType.h"

#ifndef TILEDATA_H
#define TILEDATA_H

class TileData : public QObject {

    Q_OBJECT

public:
    TileData() : _hasFlag(false), _hasEvidence(false), _type(new EvidenceType), _hint(-1), _flipped(false), _description(""), _property(-1), _suspect(-1) {}

    Q_PROPERTY(bool hasFlag READ hasFlag WRITE setHasFlag NOTIFY hasFlagChanged)
    bool hasFlag() const { return _hasFlag; }

    Q_PROPERTY(bool hasEvidence READ hasEvidence NOTIFY hasEvidenceChanged)
    bool hasEvidence() const { return _hasEvidence; }

    Q_PROPERTY(int hint READ hint NOTIFY hintChanged)
    int hint() const { return _hint; }

    Q_PROPERTY(EvidenceType *type READ type NOTIFY typeChanged)
    EvidenceType* type() const { return _type; }

    Q_PROPERTY(bool flipped READ flipped NOTIFY flippedChanged())
    bool flipped() const { return _flipped; }

    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    QString description() const { return _description; }

    int propertyId() const { return _property; }
    int suspectId() const { return _suspect; }

    void setHasFlag(bool flag) {if(flag==_hasFlag) return; _hasFlag = flag; emit hasFlagChanged();}
    void setHasEvidence(bool evidence) {if(evidence==_hasEvidence) return; _hasEvidence = evidence; emit hasEvidenceChanged();}
    void setHint(int hint) { if(hint == _hint) return; _hint = hint; emit hintChanged(); }
    void setType(EvidenceType* type) { _type = type; emit typeChanged(); }
    void flip() { if (_flipped) return; _flipped = true; emit flippedChanged(); }
    void unflip() { if(!_flipped) return; _flipped = false; emit flippedChanged(); }
    void setDescription(QString description) { _description = description; emit descriptionChanged(); }
    void setPropertyId(int property) { _property = property; emit propertyChanged(); }
    void setSuspectId(int suspect) { _suspect = suspect; emit suspectChanged(); }

signals:
    void flippedChanged();
    void hasFlagChanged();
    void hintChanged();
    void typeChanged();
    void hasEvidenceChanged();
    void descriptionChanged();
    void propertyChanged();
    void suspectChanged();

private:
    bool _hasFlag;
    bool _hasEvidence;
    EvidenceType *_type;
    int _hint;
    bool _flipped;
    QString _description;
    int _property;
    int _suspect;
};

#endif // TILEDATA_H
