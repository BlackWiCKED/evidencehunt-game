#include <QObject>
#include <QtNetwork/QNetworkReply>

#ifndef SCOREBOARD_H
#define SCOREBOARD_H

class Scoreboard : public QObject {

    Q_OBJECT

public:
    Scoreboard();
    void getList();
    void submitScore(QString, int);

public slots:
    void fileIsReady(QNetworkReply *);
    void postIsReady(QNetworkReply *);
    void authenticate(QNetworkReply*, QAuthenticator*);

signals:
    void boardChanged(QList< QHash<QString,QString> >);
    void boardError();
    void boardSubmitSuccess(int);
    void boardSubmitError();

private:
    QUrl _script;
    QNetworkAccessManager* _manager;
    int _score;

};

#endif // SCOREBOARD_H
