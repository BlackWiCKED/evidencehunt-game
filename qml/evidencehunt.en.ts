<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>EvidencehuntGame</name>
    <message>
        <location filename="../EvidenceHunt.cpp" line="174"/>
        <source>__SHOCKINGMURDER__</source>
        <translation>Shocking murder</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="175"/>
        <source>__MURDERWEAPON__</source>
        <translation>Murder weapon</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="176"/>
        <source>__ELIMINATIONCLUE__</source>
        <translation>Eliminating clue</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="177"/>
        <source>__INCRIMINATINGCLUE__</source>
        <translation>Incriminating clue</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="178"/>
        <source>__HIGHPROFILE__</source>
        <translation>High profile</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="179"/>
        <source>__AIRTIGHTALIBI__</source>
        <translation>Airtight alibi</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="180"/>
        <source>__SEARCHWARRANT__</source>
        <translation>Search warrant</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="181"/>
        <source>__SUSPECTINFO__</source>
        <translation>Suspect information</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="182"/>
        <source>__LARGEREWARD__</source>
        <translation>Large reward</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="203"/>
        <source>__SUSPECTNUMBER__</source>
        <translation>You have %1 suspects</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="210"/>
        <source>__FOUNDMURDERWEAPON__</source>
        <translation>You have found the murder weapon</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="210"/>
        <location filename="../EvidenceHunt.cpp" line="238"/>
        <source>__YOURREWARD__</source>
        <translation>Your reward is $%1!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="214"/>
        <location filename="../EvidenceHunt.cpp" line="218"/>
        <source>__KILLER__</source>
        <translation>The killer</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="222"/>
        <source>__DOUBLEREWARD__</source>
        <translation>Your income for finding clues is doubled!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="226"/>
        <source>__HASALIBI__</source>
        <translation>You discovered that %1 is innocent and has an Airtight Alibi</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="230"/>
        <source>__EXTRATIME__</source>
        <translation>You have +5 time to investigate the crime scene!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="238"/>
        <source>__FOUNDLARGEREWARD__</source>
        <translation>You gain a large reward!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="570"/>
        <source>__KEEPTHISWAY__</source>
        <translation>Keep it up</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="571"/>
        <source>__CONGRATS__</source>
        <translation>Congratulations</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="571"/>
        <source>__FOUNDTHEMURDERER__</source>
        <translation>You have successfully found the murderer!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="582"/>
        <source>__NEWPERSONALBEST__</source>
        <translation>New personal highscore</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="584"/>
        <source>__TERRIBLEMISTAKE</source>
        <translation>Terrible mistake</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="584"/>
        <source>__MISSEDTHEMURDERER__</source>
        <translation>You have accused the wrong person!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="649"/>
        <source>__TIMEISUP__</source>
        <translation>Time is up</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="649"/>
        <source>__TIMETOSOLVE__</source>
        <translation>It&apos;s time to solve the case!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="711"/>
        <source>__PLAYING__</source>
        <translation>I&apos;m playing EvidenceHunt Game on my Nokia N9/N950!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="726"/>
        <source>__PLAYER__</source>
        <translation>Player</translation>
    </message>
</context>
<context>
    <name>GameMenuBottom</name>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="27"/>
        <source>__TOPLIST__</source>
        <translation>Toplist</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="56"/>
        <source>__RESTART__</source>
        <translation>Restart</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="56"/>
        <source>__NEXTCASE__</source>
        <translation>Next case</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="82"/>
        <source>__CANCEL__</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="82"/>
        <source>__SOLVE__</source>
        <translation>Solve the case</translation>
    </message>
</context>
<context>
    <name>GameMenuTop</name>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="30"/>
        <source>__SHARE__</source>
        <translation>Share</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="57"/>
        <source>__SETTINGS__</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="82"/>
        <source>__ABOUT__</source>
        <translation>Guide</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="107"/>
        <source>__QUIT__</source>
        <translation>Quit</translation>
    </message>
</context>
<context>
    <name>Score</name>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Money</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Score.qml" line="20"/>
        <source>__SCORE__</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Score.qml" line="31"/>
        <source>__TIME__</source>
        <translation>Days</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Score.qml" line="48"/>
        <source>__FOUND__</source>
        <translation>Found</translation>
    </message>
</context>
<context>
    <name>Scoreboard</name>
    <message>
        <source>__RESET__</source>
        <translation type="obsolete">Reset</translation>
    </message>
    <message>
        <source>__RESETQUESTION__</source>
        <translation type="obsolete">Do you really want to reset the current game?</translation>
    </message>
    <message>
        <source>__CANCEL__</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="25"/>
        <source>__TOPALL__</source>
        <translation>All time</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="43"/>
        <source>__TOPMONTH__</source>
        <translation>This month</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="62"/>
        <source>__TOPWEEK__</source>
        <translation>This week</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="81"/>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="212"/>
        <source>__LOADING__</source>
        <translation>Loading scoreboard... please wait!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="88"/>
        <source>__RETRY__</source>
        <translation>Retry</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="112"/>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="119"/>
        <source>__LASTSCORE__</source>
        <translation>Current score</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="125"/>
        <source>__BESTSCORE__</source>
        <translation>Best score</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="131"/>
        <source>__WINNINGSTREAK__</source>
        <translation>Winning streak</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="137"/>
        <source>__FINISHFIRST__</source>
        <translation>Finish the current game first!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="143"/>
        <source>__WASSUBMITTED__</source>
        <translation>Your best score was submitted!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="172"/>
        <source>__SUBMIT__</source>
        <translation>Submit</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="197"/>
        <source>__ONLINE__</source>
        <translation>Online scoreboard</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="228"/>
        <source>__CONNECTIONERROR__</source>
        <translation>Unable to get the scoreboard.</translation>
    </message>
</context>
<context>
    <name>Sheet</name>
    <message>
        <source>__ABOUT__</source>
        <translation type="obsolete">Guide</translation>
    </message>
    <message>
        <location filename="DesktopComponents/Sheet.qml" line="50"/>
        <source>__CLOSE__</source>
        <translation>Close</translation>
    </message>
</context>
<context>
    <name>Suspect</name>
    <message>
        <location filename="EvidencehuntCore/Suspect.qml" line="83"/>
        <source>__SUSPECT__</source>
        <translation>Suspect</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="12"/>
        <source>__GAMEGOALTITLE__</source>
        <translation>The goal of the game</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="20"/>
        <source>__GAMEGOALTEXT__</source>
        <translation>EvidenceHunt is a role-playing logical puzzle game. The main character, in this case you, is a detective, who must solve various crime scene mysteries. There are many clues at your disposal to help you get your work done. Every puzzle has a solution and it can be reached by using logic only, of course you can guess, but be careful, a wrong decision can end your career!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="27"/>
        <source>__GAMEPLAYTITLE__</source>
        <translation>Playing the game</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="37"/>
        <source>__GAMEPLAY1__</source>
        <translation>At the very start of every game, the player gets details about the murder scene and a list of possible suspects. Depending on the player&apos;s experience, a number of fields are revealed at the start of the investigation. These fields are not hiding any evidence, but they can help you to find nearby clues.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="47"/>
        <source>__GAMEPLAY2__</source>
        <translation>To successfully solve the case, it is essential to exclude innocent suspects from among the possible perpetrators. If you play cleverly, there will be only one suspect at the end of the investigation.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="59"/>
        <source>__GAMEPLAY3__</source>
        <translation>The detective has a certain number of days to solve the case. Only one location can be searched in a single day, the game tracks the time you have left. If you run out of time, you can use the clues you&apos;ve found to deduce the killer&apos;s identity. You may end up having to guess between suspects if you haven&apos;t uncovered enough evidence. Try to investigate carefully and find every hidden clue.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="69"/>
        <source>__GAMEPLAY4__</source>
        <translation>The player gets only one guess. Just one mistake, and the game is over - but don&apos;t worry, you can start a new game anytime!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="78"/>
        <source>__GAMEINTERFACETITLE__</source>
        <translation>The game interface</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="98"/>
        <source>__GAMEINTERFACETEXT__</source>
        <translation>The investigation area is represented by a 8x8 jigsaw puzzle. Several elements will already be revealed at the start of the game. Every piece has a number in it, this number shows how many clues are hiding nearby. Different colours represent different information:

White – Locations that have not been searched
Red – The crime scene and the murder weapon
Purple – Useful information or a reward
Yellow – Suspect information
Blue – Eliminating clue
Green – Incriminating clue
Black – Location without evidence</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="105"/>
        <source>__HOWTOPLAYTITLE__</source>
        <translation>How to play</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="116"/>
        <source>__HOWTOPLAY1__</source>
        <translation>There are two options available on any investigation area. If you want to search a location, just tap the desired jigsaw piece. However, if you wish to exclude a location from further investigation, you need to tap and hold down for a while. In this case a red X will appear marking the location as excluded. Another tap and hold will bring back the original state.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="125"/>
        <source>__HOWTOPLAY2__</source>
        <translation>Re-investigating a location alreader searched does not require another day. The detective just reads the investigation report from that location, there&apos;s no need to search again.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="134"/>
        <source>__HOWTOPLAY3__</source>
        <translation>The list of suspects works in a similar way. To exclude a suspect you will need to tap and hold on its profile picture. This can be undone the same way. To avoid accidental accusations a single tap will not accuse the suspect of murder!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="153"/>
        <source>__HOWTOPLAY4__</source>
        <translation>To guess the murderer use the “Solve the case” button. By doing this, you activate suspicion mode. When suspicion mode is active, a single tap on the suspect&apos;s profile picture will accuse the suspect. If you change your mind, and want to continue with the investigation instead, use the “Cancel” button, and the game will return to investigation mode.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="163"/>
        <source>__GOODLUCK__</source>
        <translation>Good luck and have fun!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="184"/>
        <source>__BLACKWICKED__</source>
        <translation>Edvin Rab
Developer</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="201"/>
        <source>__BLACKFAIRY__</source>
        <translation>Karolina Kalocsai
Serbian translation</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="217"/>
        <source>__DRU__</source>
        <translation>Dru Moore
English translation</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="233"/>
        <source>__ALESSANDRO__</source>
        <translation>Alessandro Pra&apos;
Italian translation</translation>
    </message>
</context>
<context>
    <name>blackberry</name>
    <message>
        <location filename="blackberry.qml" line="16"/>
        <source>__EXIT__</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="26"/>
        <source>__EXITQUESTION__</source>
        <translation>Do you really want to leave EvidenceHunt the Game?</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="35"/>
        <location filename="blackberry.qml" line="60"/>
        <source>__CANCEL__</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="41"/>
        <source>__RESET__</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="51"/>
        <source>__RESETQUESTION__</source>
        <translation>Do you really want to reset the current game?</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="66"/>
        <source>__ABOUTWELCOME__</source>
        <translation>Welcome to EvidenceHunt the Game!</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="74"/>
        <source>__ABOUTWELCOMETEXT__</source>
        <translation>
On the next few pages you will find an introduction to the game rules, and instructions on how to play effectively and efficiently.

Before we start please select the game language.

Currently you can choose between 4 languages:
English, Hungarian, Serbian and Italian.
</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="81"/>
        <location filename="blackberry.qml" line="154"/>
        <source>__LANGUAGE__</source>
        <translation>Select language</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="82"/>
        <source>__CONTINUE__</source>
        <translation>Continue</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="84"/>
        <location filename="blackberry.qml" line="247"/>
        <source>__CLOSE__</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="90"/>
        <source>__TOPLIST__</source>
        <translation>Scoreboard</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="99"/>
        <source>__ABOUT__</source>
        <translation>Guide</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="173"/>
        <source>__SHARE__</source>
        <translation>Share</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="187"/>
        <source>__SHAREINFO__</source>
        <translation>Please share EvidenceHunt the Game with your friends on well known social networks. Thank you very much!
If you like the game, please join our Facebook Group!</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="204"/>
        <source>__OFFICIALFACEBOOK__</source>
        <translation>Official Facebook Page</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="230"/>
        <source>__FOLLOWTWITTER__</source>
        <translation>Follow The Author On Twitter</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="288"/>
        <source>__SUBMITPERSONALBEST__</source>
        <translation>Submit your best score</translation>
    </message>
</context>
<context>
    <name>desktop</name>
    <message>
        <source>__SETTINGS__</source>
        <translation type="obsolete">Settings</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="98"/>
        <source>__ABOUT__</source>
        <translation>About</translation>
    </message>
    <message>
        <source>__QUIT__</source>
        <translation type="obsolete">Quit</translation>
    </message>
    <message>
        <source>__FINDMURDERER__</source>
        <translation type="obsolete">Find the murderer</translation>
    </message>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Money</translation>
    </message>
    <message>
        <source>__TIME__</source>
        <translation type="obsolete">Time</translation>
    </message>
    <message>
        <source>__SOLVE__</source>
        <translation type="obsolete">Solve the case</translation>
    </message>
    <message>
        <source>__RESTART__</source>
        <translation type="obsolete">Restart the game</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="15"/>
        <source>__EXIT__</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="25"/>
        <source>__EXITQUESTION__</source>
        <translation>Do you really want to leave EvidenceHunt the Game?</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="34"/>
        <location filename="desktop.qml" line="59"/>
        <source>__CANCEL__</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="40"/>
        <source>__RESET__</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="50"/>
        <source>__RESETQUESTION__</source>
        <translation>Do you really want to reset the current game?</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="65"/>
        <source>__ABOUTWELCOME__</source>
        <translation>Welcome to EvidenceHunt the Game!</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="73"/>
        <source>__ABOUTWELCOMETEXT__</source>
        <translation>
On the next few pages you will find an introduction to the game rules, and instructions on how to play effectively and efficiently.

Before we start please select the game language.

Currently you can choose between 4 languages:
English, Hungarian, Serbian and Italian.
</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="80"/>
        <location filename="desktop.qml" line="153"/>
        <source>__LANGUAGE__</source>
        <translation>Select language</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="81"/>
        <source>__CONTINUE__</source>
        <translation>Continue</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="83"/>
        <location filename="desktop.qml" line="246"/>
        <source>__CLOSE__</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="89"/>
        <source>__TOPLIST__</source>
        <translation>Scoreboard</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="172"/>
        <source>__SHARE__</source>
        <translation>Share</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="186"/>
        <source>__SHAREINFO__</source>
        <translation>Please share EvidenceHunt the Game with your friends on well known social networks. Thank you very much!
If you like the game, please join our Facebook Group!</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="203"/>
        <source>__OFFICIALFACEBOOK__</source>
        <translation>Official Facebook Page</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="229"/>
        <source>__FOLLOWTWITTER__</source>
        <translation>Follow The Author On Twitter</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="286"/>
        <source>__SUBMITPERSONALBEST__</source>
        <translation>Submit your best score</translation>
    </message>
</context>
<context>
    <name>evidencehunt</name>
    <message>
        <source>__SETTINGS__</source>
        <oldsource>Settings</oldsource>
        <translation type="obsolete">Settings</translation>
    </message>
    <message>
        <source>__ABOUT__</source>
        <oldsource>About</oldsource>
        <translation type="obsolete">About</translation>
    </message>
    <message>
        <source>__QUIT__</source>
        <oldsource>Quit</oldsource>
        <translation type="obsolete">Quit</translation>
    </message>
    <message>
        <source>__FINDMURDERER__</source>
        <oldsource>Find the murderer</oldsource>
        <translation type="obsolete">Find the murderer</translation>
    </message>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Money</translation>
    </message>
    <message>
        <source>Score</source>
        <translation type="obsolete">Score</translation>
    </message>
    <message>
        <source>__TIME__</source>
        <oldsource>Time</oldsource>
        <translation type="obsolete">Time</translation>
    </message>
    <message>
        <source>__SOLVE__</source>
        <translation type="obsolete">Solve the case</translation>
    </message>
    <message>
        <source>__RESTART__</source>
        <oldsource>Restart the game!</oldsource>
        <translation type="obsolete">Restart the game</translation>
    </message>
</context>
<context>
    <name>harmattan</name>
    <message>
        <source>__SETTINGS__</source>
        <translation type="obsolete">Settings</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="62"/>
        <source>__ABOUT__</source>
        <translation>Guide</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="38"/>
        <location filename="harmattan.qml" line="123"/>
        <source>__LANGUAGE__</source>
        <translation>Select language</translation>
    </message>
    <message>
        <source>__ENGLISH__</source>
        <translation type="obsolete">English</translation>
    </message>
    <message>
        <source>__HUNGARIAN__</source>
        <translation type="obsolete">Hungarian</translation>
    </message>
    <message>
        <source>__SERBIAN__</source>
        <translation type="obsolete">Serbian</translation>
    </message>
    <message>
        <source>__QUIT__</source>
        <translation type="obsolete">Quit</translation>
    </message>
    <message>
        <source>__FINDMURDERER__</source>
        <translation type="obsolete">Find the murderer</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="142"/>
        <location filename="harmattan.qml" line="206"/>
        <source>__SHARE__</source>
        <translation>Share</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="150"/>
        <source>__SHAREINFO__</source>
        <translation>Please share EvidenceHunt the Game with your friends on well known
social networks. Thank you very much!
If you like the game, please join our Facebook Group!</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="167"/>
        <source>__OFFICIALFACEBOOK__</source>
        <translation>Official Facebook Page</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="193"/>
        <source>__FOLLOWTWITTER__</source>
        <translation>Follow The Author On Twitter</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="41"/>
        <location filename="harmattan.qml" line="47"/>
        <location filename="harmattan.qml" line="61"/>
        <location filename="harmattan.qml" line="207"/>
        <source>__CLOSE__</source>
        <translation>Close</translation>
    </message>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Money</translation>
    </message>
    <message>
        <source>__TIME__</source>
        <translation type="obsolete">Days</translation>
    </message>
    <message>
        <source>__FOUND__</source>
        <translation type="obsolete">Found</translation>
    </message>
    <message>
        <source>__SOLVE__</source>
        <translation type="obsolete">Solve the case</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="17"/>
        <location filename="harmattan.qml" line="27"/>
        <source>__CANCEL__</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="13"/>
        <source>__EXIT__</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="14"/>
        <source>__EXITQUESTION__</source>
        <translation>Do you really want to leave EvidenceHunt the Game?</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="23"/>
        <source>__RESET__</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="24"/>
        <source>__RESETQUESTION__</source>
        <translation>Do you really want to reset the current game?</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="34"/>
        <source>__ABOUTWELCOME__</source>
        <translation>Welcome to EvidenceHunt the Game!</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="35"/>
        <source>__ABOUTWELCOMETEXT__</source>
        <translation>
On the next few pages you will find an introduction to the game rules,
and instructions on how to play effectively and efficiently.

Before we start please select the game language.

Currently you can choose between 4 languages:
English, Hungarian, Serbian and Italian.
</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="39"/>
        <source>__CONTINUE__</source>
        <translation>Continue</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="48"/>
        <source>__TOPLIST__</source>
        <translation>Scoreboard</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="248"/>
        <source>__SUBMITPERSONALBEST__</source>
        <translation>Submit your best score</translation>
    </message>
    <message>
        <source>__GAMEGOALTITLE__</source>
        <translation type="obsolete">The goal of the game</translation>
    </message>
    <message>
        <source>__GAMEGOALTEXT__</source>
        <translation type="obsolete">EvidenceHunt is a role-playing logical puzzle game. The main character, in this case you, is a detective, who must solve various crime scene mysteries. There are many clues at your disposal to help you get your work done. Every puzzle has a solution and it can be reached by using logic only, of course you can guess, but be careful, a wrong decision can end your career!</translation>
    </message>
    <message>
        <source>__GAMEPLAYTITLE__</source>
        <translation type="obsolete">Playing the game</translation>
    </message>
    <message>
        <source>__GAMEPLAY1__</source>
        <translation type="obsolete">At the very start of every game, the player gets details about the murder scene and a list of possible suspects. Depending on the player&apos;s experience, a number of fields are revealed at the start of the investigation. These fields are not hiding any evidence, but they can help you to find nearby clues.</translation>
    </message>
    <message>
        <source>__GAMEPLAY2__</source>
        <translation type="obsolete">To successfully solve the case, it is essential to exclude innocent suspects from among the possible perpetrators. If you play cleverly, there will be only one suspect at the end of the investigation.</translation>
    </message>
    <message>
        <source>__GAMEPLAY3__</source>
        <translation type="obsolete">The detective has a certain number of days to solve the case. Only one location can be searched in a single day, the game tracks the time you have left. If you run out of time, you can use the clues you&apos;ve found to deduce the killer&apos;s identity. You may end up having to guess between suspects if you haven&apos;t uncovered enough evidence. Try to investigate carefully and find every hidden clue.</translation>
    </message>
    <message>
        <source>__GAMEPLAY4__</source>
        <translation type="obsolete">The player gets only one guess. Just one mistake, and the game is over - but don&apos;t worry, you can start a new game anytime!</translation>
    </message>
    <message>
        <source>__GAMEINTERFACETITLE__</source>
        <translation type="obsolete">The game interface</translation>
    </message>
    <message>
        <source>__GAMEINTERFACETEXT__</source>
        <translation type="obsolete">The investigation area is represented by a 8x8 jigsaw puzzle. Several elements will already be revealed at the start of the game. Every piece has a number in it, this number shows how many clues are hiding nearby. Different colours represent different information:

White – Locations that have not been searched
Red – The crime scene and the murder weapon
Purple – Useful information or a reward
Yellow – Suspect information
Blue – Eliminating clue
Green – Incriminating clue
Black – Location without evidence</translation>
    </message>
    <message>
        <source>__HOWTOPLAYTITLE__</source>
        <translation type="obsolete">How to play</translation>
    </message>
    <message>
        <source>__HOWTOPLAY1__</source>
        <translation type="obsolete">There are two options available on any investigation area. If you want to search a location, just tap the desired jigsaw piece. However, if you wish to exclude a location from further investigation, you need to tap and hold down for a while. In this case a red X will appear marking the location as excluded. Another tap and hold will bring back the original state.</translation>
    </message>
    <message>
        <source>__HOWTOPLAY2__</source>
        <translation type="obsolete">Re-investigating a location alreader searched does not require another day. The detective just reads the investigation report from that location, there&apos;s no need to search again.</translation>
    </message>
    <message>
        <source>__HOWTOPLAY3__</source>
        <translation type="obsolete">The list of suspects works in a similar way. To exclude a suspect you will need to tap and hold on its profile picture. This can be undone the same way. To avoid accidental accusations a single tap will not accuse the suspect of murder!</translation>
    </message>
    <message>
        <source>__HOWTOPLAY4__</source>
        <translation type="obsolete">To guess the murderer use the “Solve the case” button. By doing this, you activate suspicion mode. When suspicion mode is active, a single tap on the suspect&apos;s profile picture will accuse the suspect. If you change your mind, and want to continue with the investigation instead, use the “Cancel” button, and the game will return to investigation mode.</translation>
    </message>
    <message>
        <source>__GOODLUCK__</source>
        <translation type="obsolete">Good luck and have fun!</translation>
    </message>
    <message>
        <source>__BLACKWICKED__</source>
        <translation type="obsolete">Edvin Rab
Developer</translation>
    </message>
    <message>
        <source>__BLACKFAIRY__</source>
        <translation type="obsolete">Karolina Kalocsai
Serbian translation</translation>
    </message>
    <message>
        <source>__DRU__</source>
        <translation type="obsolete">Dru Moore
English translation</translation>
    </message>
    <message>
        <source>__ALESSANDRO__</source>
        <translation type="obsolete">Alessandro Pra&apos;
Italian translation</translation>
    </message>
    <message>
        <source>__RESTART__</source>
        <translation type="obsolete">Restart the game</translation>
    </message>
</context>
</TS>
