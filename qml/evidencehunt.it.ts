<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it_IT">
<context>
    <name>EvidencehuntGame</name>
    <message>
        <location filename="../EvidenceHunt.cpp" line="174"/>
        <source>__SHOCKINGMURDER__</source>
        <translation>Omicidio scioccante</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="175"/>
        <source>__MURDERWEAPON__</source>
        <translation>Arma del delitto</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="176"/>
        <source>__ELIMINATIONCLUE__</source>
        <translation>Indizio scagionante</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="177"/>
        <source>__INCRIMINATINGCLUE__</source>
        <translation>Indizio incriminante</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="178"/>
        <source>__HIGHPROFILE__</source>
        <translation>Alto profilo</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="179"/>
        <source>__AIRTIGHTALIBI__</source>
        <translation>Alibi perfetto</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="180"/>
        <source>__SEARCHWARRANT__</source>
        <translation>Mandato di perquisizione</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="181"/>
        <source>__SUSPECTINFO__</source>
        <translation>Informazioni sul sospettato</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="182"/>
        <source>__LARGEREWARD__</source>
        <translation>Grossa ricompensa</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="203"/>
        <source>__SUSPECTNUMBER__</source>
        <translation>Hai %1 sospetti</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="210"/>
        <source>__FOUNDMURDERWEAPON__</source>
        <translation>Hai trovato l&apos;arma del delitto</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="210"/>
        <location filename="../EvidenceHunt.cpp" line="238"/>
        <source>__YOURREWARD__</source>
        <translation>La tua ricompensa è di %1 $!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="214"/>
        <location filename="../EvidenceHunt.cpp" line="218"/>
        <source>__KILLER__</source>
        <translation>L&apos;assassino</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="222"/>
        <source>__DOUBLEREWARD__</source>
        <translation>I tuoi introiti per gli indizi trovati sono raddoppiati!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="226"/>
        <source>__HASALIBI__</source>
        <translation>Hai scoperto che %1 è innocente e ha un Alibi Perfetto</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="230"/>
        <source>__EXTRATIME__</source>
        <translation>Hai +5 giorni per ispezionare la scena del crimine!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="238"/>
        <source>__FOUNDLARGEREWARD__</source>
        <translation>Guadagni una grossa ricompensa!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="570"/>
        <source>__KEEPTHISWAY__</source>
        <translation>Continua così</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="571"/>
        <source>__CONGRATS__</source>
        <translation>Congratulazioni</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="571"/>
        <source>__FOUNDTHEMURDERER__</source>
        <translation>Hai trovato l&apos;assassino!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="582"/>
        <source>__NEWPERSONALBEST__</source>
        <translation>Nuovo record personale</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="584"/>
        <source>__TERRIBLEMISTAKE</source>
        <translation>Errore tremendo</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="584"/>
        <source>__MISSEDTHEMURDERER__</source>
        <translation>Hai accusato la persona sbagliata!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="649"/>
        <source>__TIMEISUP__</source>
        <translation>Il tempo è scaduto</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="649"/>
        <source>__TIMETOSOLVE__</source>
        <translation>È il momento di risolvere il caso!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="711"/>
        <source>__PLAYING__</source>
        <translation>Sto giocando a EvidenceHunt Game sul mio Nokia N9/N950!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="726"/>
        <source>__PLAYER__</source>
        <translation>Giocatore</translation>
    </message>
</context>
<context>
    <name>GameMenuBottom</name>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="27"/>
        <source>__TOPLIST__</source>
        <translation>Classifica</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="56"/>
        <source>__RESTART__</source>
        <translation>Ricomincia</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="56"/>
        <source>__NEXTCASE__</source>
        <translation>Prossimo caso</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="82"/>
        <source>__CANCEL__</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="82"/>
        <source>__SOLVE__</source>
        <translation>Risolvi il caso</translation>
    </message>
</context>
<context>
    <name>GameMenuTop</name>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="30"/>
        <source>__SHARE__</source>
        <translation>Condividi</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="57"/>
        <source>__SETTINGS__</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="82"/>
        <source>__ABOUT__</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="107"/>
        <source>__QUIT__</source>
        <translation>Esci</translation>
    </message>
</context>
<context>
    <name>Score</name>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Denaro</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Score.qml" line="20"/>
        <source>__SCORE__</source>
        <translation>Punti</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Score.qml" line="31"/>
        <source>__TIME__</source>
        <translation>Giorni</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Score.qml" line="48"/>
        <source>__FOUND__</source>
        <translation>Trovato</translation>
    </message>
</context>
<context>
    <name>Scoreboard</name>
    <message>
        <source>__RESET__</source>
        <translation type="obsolete">Resetta</translation>
    </message>
    <message>
        <source>__RESETQUESTION__</source>
        <translation type="obsolete">Vuoi davvero resettare la partita attuale?</translation>
    </message>
    <message>
        <source>__CANCEL__</source>
        <translation type="obsolete">Annulla</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="25"/>
        <source>__TOPALL__</source>
        <translation>Sempre</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="43"/>
        <source>__TOPMONTH__</source>
        <translation>Questo mese</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="62"/>
        <source>__TOPWEEK__</source>
        <translation>Questa settimana</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="81"/>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="212"/>
        <source>__LOADING__</source>
        <translation>Caricamento della classifica... attendere prego!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="88"/>
        <source>__RETRY__</source>
        <translation>Riprova</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="112"/>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="119"/>
        <source>__LASTSCORE__</source>
        <translation>Punteggio attuale</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="125"/>
        <source>__BESTSCORE__</source>
        <translation>Miglior punteggio</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="131"/>
        <source>__WINNINGSTREAK__</source>
        <translation>Vittorie consecutive</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="137"/>
        <source>__FINISHFIRST__</source>
        <translation>Finisci la partita attuale prima!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="143"/>
        <source>__WASSUBMITTED__</source>
        <translation>Il tuo miglior punteggio è stato inviato!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="172"/>
        <source>__SUBMIT__</source>
        <translation>Invia</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="197"/>
        <source>__ONLINE__</source>
        <translation>Classifica online</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="228"/>
        <source>__CONNECTIONERROR__</source>
        <translation>Impossibile scaricare la classifica.</translation>
    </message>
</context>
<context>
    <name>Sheet</name>
    <message>
        <source>__ABOUT__</source>
        <translation type="obsolete">Guida</translation>
    </message>
    <message>
        <location filename="DesktopComponents/Sheet.qml" line="50"/>
        <source>__CLOSE__</source>
        <translation>Chiudi</translation>
    </message>
</context>
<context>
    <name>Suspect</name>
    <message>
        <location filename="EvidencehuntCore/Suspect.qml" line="83"/>
        <source>__SUSPECT__</source>
        <translation>Sospetto</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="12"/>
        <source>__GAMEGOALTITLE__</source>
        <translation>Lo scopo del gioco</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="20"/>
        <source>__GAMEGOALTEXT__</source>
        <translation>EvidenceHunt è un rompicapo a tema investigativo. Impersonerai un detective, il cui compito è risolvere alcuni casi di omicidio. Hai molti indizi a tua disposizione per aiutarti a portare a termine il tuo lavoro. Ogni rompicapo può essere risolto usando solo la logica. Naturalmente puoi tirare a indovindare, ma attenzione, una decisione sbagliata può porre fine alla tua carriera!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="27"/>
        <source>__GAMEPLAYTITLE__</source>
        <translation>Descrizione del gioco</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="37"/>
        <source>__GAMEPLAY1__</source>
        <translation>All&apos;inizio di ogni partita, il giocatore riceve dettagli sulla scena del crimine e una lista di possibili sospetti. A seconda dell&apos;esperienza del giocatore, alcune caselle sono rivelate all&apos;inizio dell&apos;indagine. Queste caselle non nascondono alcuna prova, ma possono aiutarti a trovare indizi nelle caselle vicine.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="47"/>
        <source>__GAMEPLAY2__</source>
        <translation>Per risolvere il caso, è essenziale escludere dalla lista dei possibili sospetti gli innocenti. Se giocherai con astuzia, ci sarà solo un sospetto alla fine dell&apos;indagine.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="59"/>
        <source>__GAMEPLAY3__</source>
        <translation>Il detective ha un certo numero di giorni per risolvere il caso. Solo una casella può essere perlustrata in un singolo giorno, il gioco tiene traccia dei giorni che ti rimangono. Se finisci il tempo, puoi usare gli indizi che hai trovato per dedurre l&apos;identità dell&apos;assassino. Potresti finire per dover tirare a indovinare tra più sospetti se non hai raccolto prove sufficienti. Cerca di indagare attentamente e trovare ogni indizio nascosto.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="69"/>
        <source>__GAMEPLAY4__</source>
        <translation>Il giocatore ha solo un tentativo. Un solo errore, e la partita finisce - ma non preoccuparti, puoi sempre iniziare una nuova partita!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="78"/>
        <source>__GAMEINTERFACETITLE__</source>
        <translation>L&apos;interfaccia di gioco</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="98"/>
        <source>__GAMEINTERFACETEXT__</source>
        <translation>L&apos;area da investigare è rappresentata da un puzzle 8x8. Diverse caselle saranno già rivelate all&apos;inizio della partita. Ogni casella ha un numero al suo interno, che indica quanti indizi si nascondono nelle caselle circostanti. I differenti colori indicano i diversi tipi di informazioni:

Bianco – Caselle che non sono state ispezionate
Rosso – La scena del crimine e l&apos;arma del delitto
Viola – Informazioni utili o una ricompensa
Giallo – Informazioni su un sospetto
Blu – Indizio scagionante
Verde – Indizio incriminante
Nero – Casella senza indizi</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="105"/>
        <source>__HOWTOPLAYTITLE__</source>
        <translation>Come giocare</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="116"/>
        <source>__HOWTOPLAY1__</source>
        <translation>Per ogni casella ci sono due opzioni a disposizione durante l&apos;investigazione. Se vuoi perlustrare una casella, devi solo toccarla brevemente. Tuttavia, se desideri escludere una casella da ogni indagine futura, devi toccarla più a lungo senza sollevare il dito. In questo caso comparirà una X rossa, questo indica che la casella è esclusa. Un altro tocco prolungato riporterà la casella allo stato originale.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="125"/>
        <source>__HOWTOPLAY2__</source>
        <translation>Perlustrare una casella già scoperta non richiede un altro giorno. Il detective può leggere il resoconto dell&apos;investigazione di quella casella, non c&apos;è bisogno di perlustrarla di nuovo.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="134"/>
        <source>__HOWTOPLAY3__</source>
        <translation>La lista dei sospetti funziona allo stesso modo. Per esludere un sospetto devi toccare la sua foto e tenere giù il dito per qualche istante; allo stesso modo puoi riportarlo allo stato originale. Per evitare accuse accidentali, un singolo tocco non accuserà il sospetto di omicidio!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="153"/>
        <source>__HOWTOPLAY4__</source>
        <translation>Per accusare l&apos;assassino usa il bottone &quot;Risolvi il caso&quot;. Quando la modalità accusa è attiva, un singolo tocco sulla foto di un sospetto produrrà un&apos;accusa nei suoi confronti. Se invece cambi idea, e vuoi continuare a indagare, usa il tasto &quot;Annulla&quot;, e il gioco tornerà alla modalità investigazione.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="163"/>
        <source>__GOODLUCK__</source>
        <translation>Buona fortuna e buon divertimento!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="184"/>
        <source>__BLACKWICKED__</source>
        <translation>Edvin Rab
Sviluppatore</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="201"/>
        <source>__BLACKFAIRY__</source>
        <translation>Karolina Kalocsai
Traduzione serba</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="217"/>
        <source>__DRU__</source>
        <translation>Dru Moore
Traduzione inglese</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="233"/>
        <source>__ALESSANDRO__</source>
        <translation>Alessandro Pra&apos;
Traduzione italiana</translation>
    </message>
</context>
<context>
    <name>blackberry</name>
    <message>
        <location filename="blackberry.qml" line="16"/>
        <source>__EXIT__</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="26"/>
        <source>__EXITQUESTION__</source>
        <translation>Vuoi davvero uscire da EvidenceHunt?</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="35"/>
        <location filename="blackberry.qml" line="60"/>
        <source>__CANCEL__</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="41"/>
        <source>__RESET__</source>
        <translation>Resetta</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="51"/>
        <source>__RESETQUESTION__</source>
        <translation>Vuoi davvero resettare la partita attuale?</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="66"/>
        <source>__ABOUTWELCOME__</source>
        <translation>Benvenuto a EvidenceHunt!</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="74"/>
        <source>__ABOUTWELCOMETEXT__</source>
        <translation>
Nelle prossime pagine troverai un&apos;introduzione alle regole del gioco, e istruzioni su come giocare con efficacia.

Prima di iniziare per favore scegli la lingua di gioco.

Al momento puoi scegliere tra 4 lingue:
Inglese, Ungherese, Serbo e Italiano.
</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="81"/>
        <location filename="blackberry.qml" line="154"/>
        <source>__LANGUAGE__</source>
        <translation>Seleziona lingua</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="82"/>
        <source>__CONTINUE__</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="84"/>
        <location filename="blackberry.qml" line="247"/>
        <source>__CLOSE__</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="90"/>
        <source>__TOPLIST__</source>
        <translation>Classifica</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="99"/>
        <source>__ABOUT__</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="173"/>
        <source>__SHARE__</source>
        <translation>Condividi</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="187"/>
        <source>__SHAREINFO__</source>
        <translation>Per favore condividi EvidenceHunt con i tuoi amici sui più conosciuti social network. Molte grazie!
Se ti è piaciuto il gioco, per favore unisciti al nostro Gruppo Facebook!</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="204"/>
        <source>__OFFICIALFACEBOOK__</source>
        <translation>Pagina Facebook ufficiale</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="230"/>
        <source>__FOLLOWTWITTER__</source>
        <translation>Segui l&apos;autore su Twitter</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="288"/>
        <source>__SUBMITPERSONALBEST__</source>
        <translation>Invia il tuo miglior punteggio</translation>
    </message>
</context>
<context>
    <name>desktop</name>
    <message>
        <source>__SETTINGS__</source>
        <translation type="obsolete">Impostazioni</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="98"/>
        <source>__ABOUT__</source>
        <translation>Guida</translation>
    </message>
    <message>
        <source>__QUIT__</source>
        <translation type="obsolete">Esci</translation>
    </message>
    <message>
        <source>__FINDMURDERER__</source>
        <translation type="obsolete">Trova l&apos;assassino</translation>
    </message>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Denaro</translation>
    </message>
    <message>
        <source>__TIME__</source>
        <translation type="obsolete">Tempo</translation>
    </message>
    <message>
        <source>__SOLVE__</source>
        <translation type="obsolete">Risolvi il caso</translation>
    </message>
    <message>
        <source>__RESTART__</source>
        <translation type="obsolete">Ricomincia la partita</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="15"/>
        <source>__EXIT__</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="25"/>
        <source>__EXITQUESTION__</source>
        <translation>Vuoi davvero uscire da EvidenceHunt?</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="34"/>
        <location filename="desktop.qml" line="59"/>
        <source>__CANCEL__</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="40"/>
        <source>__RESET__</source>
        <translation>Resetta</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="50"/>
        <source>__RESETQUESTION__</source>
        <translation>Vuoi davvero resettare la partita attuale?</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="65"/>
        <source>__ABOUTWELCOME__</source>
        <translation>Benvenuto a EvidenceHunt!</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="73"/>
        <source>__ABOUTWELCOMETEXT__</source>
        <translation>
Nelle prossime pagine troverai un&apos;introduzione alle regole del gioco, e istruzioni su come giocare con efficacia.

Prima di iniziare per favore scegli la lingua di gioco.

Al momento puoi scegliere tra 4 lingue:
Inglese, Ungherese, Serbo e Italiano.
</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="80"/>
        <location filename="desktop.qml" line="153"/>
        <source>__LANGUAGE__</source>
        <translation>Seleziona lingua</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="81"/>
        <source>__CONTINUE__</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="83"/>
        <location filename="desktop.qml" line="246"/>
        <source>__CLOSE__</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="89"/>
        <source>__TOPLIST__</source>
        <translation>Classifica</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="172"/>
        <source>__SHARE__</source>
        <translation>Condividi</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="186"/>
        <source>__SHAREINFO__</source>
        <translation>Per favore condividi EvidenceHunt con i tuoi amici sui più conosciuti social network. Molte grazie!
Se ti è piaciuto il gioco, unisciti al nostro Gruppo Facebook!</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="203"/>
        <source>__OFFICIALFACEBOOK__</source>
        <translation>Pagina Facebook ufficiale</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="229"/>
        <source>__FOLLOWTWITTER__</source>
        <translation>Segui l&apos;autore su Twitter</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="286"/>
        <source>__SUBMITPERSONALBEST__</source>
        <translation>Invia il tuo miglior punteggio</translation>
    </message>
</context>
<context>
    <name>evidencehunt</name>
    <message>
        <source>__SETTINGS__</source>
        <oldsource>Settings</oldsource>
        <translation type="obsolete">Settings</translation>
    </message>
    <message>
        <source>__ABOUT__</source>
        <oldsource>About</oldsource>
        <translation type="obsolete">About</translation>
    </message>
    <message>
        <source>__QUIT__</source>
        <oldsource>Quit</oldsource>
        <translation type="obsolete">Quit</translation>
    </message>
    <message>
        <source>__FINDMURDERER__</source>
        <oldsource>Find the murderer</oldsource>
        <translation type="obsolete">Find the murderer</translation>
    </message>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Money</translation>
    </message>
    <message>
        <source>Score</source>
        <translation type="obsolete">Score</translation>
    </message>
    <message>
        <source>__TIME__</source>
        <oldsource>Time</oldsource>
        <translation type="obsolete">Time</translation>
    </message>
    <message>
        <source>__SOLVE__</source>
        <translation type="obsolete">Solve the case</translation>
    </message>
    <message>
        <source>__RESTART__</source>
        <oldsource>Restart the game!</oldsource>
        <translation type="obsolete">Restart the game</translation>
    </message>
</context>
<context>
    <name>harmattan</name>
    <message>
        <source>__SETTINGS__</source>
        <translation type="obsolete">Impostazioni</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="62"/>
        <source>__ABOUT__</source>
        <translation>Guida</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="38"/>
        <location filename="harmattan.qml" line="123"/>
        <source>__LANGUAGE__</source>
        <translation>Seleziona lingua</translation>
    </message>
    <message>
        <source>__ENGLISH__</source>
        <translation type="obsolete">English</translation>
    </message>
    <message>
        <source>__HUNGARIAN__</source>
        <translation type="obsolete">Hungarian</translation>
    </message>
    <message>
        <source>__SERBIAN__</source>
        <translation type="obsolete">Serbian</translation>
    </message>
    <message>
        <source>__QUIT__</source>
        <translation type="obsolete">Esci</translation>
    </message>
    <message>
        <source>__FINDMURDERER__</source>
        <translation type="obsolete">Find the murderer</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="142"/>
        <location filename="harmattan.qml" line="206"/>
        <source>__SHARE__</source>
        <translation>Condividi</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="150"/>
        <source>__SHAREINFO__</source>
        <translation>Per favore condividi EvidenceHunt con i tuoi amici sui più conosciuti
social network. Molte grazie!
Se ti è piaciuto il gioco, per favore unisciti al nostro Gruppo Facebook!</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="167"/>
        <source>__OFFICIALFACEBOOK__</source>
        <translation>Pagina Facebook ufficiale</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="193"/>
        <source>__FOLLOWTWITTER__</source>
        <translation>Segui l&apos;autore su Twitter</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="41"/>
        <location filename="harmattan.qml" line="47"/>
        <location filename="harmattan.qml" line="61"/>
        <location filename="harmattan.qml" line="207"/>
        <source>__CLOSE__</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Denaro</translation>
    </message>
    <message>
        <source>__TIME__</source>
        <translation type="obsolete">Giorni</translation>
    </message>
    <message>
        <source>__FOUND__</source>
        <translation type="obsolete">Trovato</translation>
    </message>
    <message>
        <source>__SOLVE__</source>
        <translation type="obsolete">Risolvi il caso</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="17"/>
        <location filename="harmattan.qml" line="27"/>
        <source>__CANCEL__</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="13"/>
        <source>__EXIT__</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="14"/>
        <source>__EXITQUESTION__</source>
        <translation>Vuoi davvero uscire da EvidenceHunt?</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="23"/>
        <source>__RESET__</source>
        <translation>Resetta</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="24"/>
        <source>__RESETQUESTION__</source>
        <translation>Vuoi davvero resettare la partita attuale?</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="34"/>
        <source>__ABOUTWELCOME__</source>
        <translation>Benvenuto a EvidenceHunt!</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="35"/>
        <source>__ABOUTWELCOMETEXT__</source>
        <translatorcomment>I modified the last part from 3 to 4 languages and added Italian</translatorcomment>
        <translation>
Nelle prossime pagine troverai un&apos;introduzione alle regole del gioco,
e istruzioni su come giocare con efficacia.

Prima di iniziare per favore scegli la lingua di gioco.

Al momento puoi scegliere tra 4 lingue:
Inglese, Ungherese, Serbo e Italiano.
</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="39"/>
        <source>__CONTINUE__</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="48"/>
        <source>__TOPLIST__</source>
        <translation>Classifica</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="248"/>
        <source>__SUBMITPERSONALBEST__</source>
        <translation>Invia il tuo miglior punteggio</translation>
    </message>
    <message>
        <source>__GAMEGOALTITLE__</source>
        <translation type="obsolete">Lo scopo del gioco</translation>
    </message>
    <message>
        <source>__GAMEGOALTEXT__</source>
        <translation type="obsolete">EvidenceHunt è un rompicapo a tema investigativo. Impersonerai un detective, il cui compito è risolvere alcuni casi di omicidio. Hai molti indizi a tua disposizione per aiutarti a portare a termine il tuo lavoro. Ogni rompicapo può essere risolto usando solo la logica. Naturalmente puoi tirare a indovindare, ma attenzione, una decisione sbagliata può porre fine alla tua carriera!</translation>
    </message>
    <message>
        <source>__GAMEPLAYTITLE__</source>
        <translation type="obsolete">Descrizione del gioco</translation>
    </message>
    <message>
        <source>__GAMEPLAY1__</source>
        <translation type="obsolete">All&apos;inizio di ogni partita, il giocatore riceve dettagli sulla scena del crimine e una lista di possibili sospetti. A seconda dell&apos;esperienza del giocatore, alcune caselle sono rivelate all&apos;inizio dell&apos;indagine. Queste caselle non nascondono alcuna prova, ma possono aiutarti a trovare indizi nelle caselle vicine.</translation>
    </message>
    <message>
        <source>__GAMEPLAY2__</source>
        <translation type="obsolete">Per risolvere il caso, è essenziale escludere dalla lista dei possibili sospetti gli innocenti. Se giocherai con astuzia, ci sarà solo un sospetto alla fine dell&apos;indagine.</translation>
    </message>
    <message>
        <source>__GAMEPLAY3__</source>
        <translation type="obsolete">Il detective ha un certo numero di giorni per risolvere il caso. Solo una casella può essere perlustrata in un singolo giorno, il gioco tiene traccia dei giorni che ti rimangono. Se finisci il tempo, puoi usare gli indizi che hai trovato per dedurre l&apos;identità dell&apos;assassino. Potresti finire per dover tirare a indovinare tra più sospetti se non hai raccolto prove sufficienti. Cerca di indagare attentamente e trovare ogni indizio nascosto.</translation>
    </message>
    <message>
        <source>__GAMEPLAY4__</source>
        <translation type="obsolete">Il giocatore ha solo un tentativo. Un solo errore, e la partita finisce - ma non preoccuparti, puoi sempre iniziare una nuova partita!</translation>
    </message>
    <message>
        <source>__GAMEINTERFACETITLE__</source>
        <translation type="obsolete">L&apos;interfaccia di gioco</translation>
    </message>
    <message>
        <source>__GAMEINTERFACETEXT__</source>
        <translation type="obsolete">L&apos;area da investigare è rappresentata da un puzzle 8x8. Diverse caselle saranno già rivelate all&apos;inizio della partita. Ogni casella ha un numero al suo interno, che indica quanti indizi si nascondono nelle caselle circostanti. I differenti colori indicano i diversi tipi di informazioni:

Bianco – Caselle che non sono state ispezionate
Rosso – La scena del crimine e l&apos;arma del delitto
Viola – Informazioni utili o una ricompensa
Giallo – Informazioni su un sospetto
Blu – Indizio scagionante
Verde – Indizio incriminante
Nero – Casella senza indizi</translation>
    </message>
    <message>
        <source>__HOWTOPLAYTITLE__</source>
        <translation type="obsolete">Come giocare</translation>
    </message>
    <message>
        <source>__HOWTOPLAY1__</source>
        <translation type="obsolete">Per ogni casella ci sono due opzioni a disposizione durante l&apos;investigazione. Se vuoi perlustrare una casella, devi solo toccarla brevemente. Tuttavia, se desideri escludere una casella da ogni indagine futura, devi toccarla più a lungo senza sollevare il dito. In questo caso comparirà una X rossa, questo indica che la casella è esclusa. Un altro tocco prolungato riporterà la casella allo stato originale.</translation>
    </message>
    <message>
        <source>__HOWTOPLAY2__</source>
        <translation type="obsolete">Perlustrare una casella già scoperta non richiede un altro giorno. Il detective può leggere il resoconto dell&apos;investigazione di quella casella, non c&apos;è bisogno di perlustrarla di nuovo.</translation>
    </message>
    <message>
        <source>__HOWTOPLAY3__</source>
        <translation type="obsolete">La lista dei sospetti funziona allo stesso modo. Per esludere un sospetto devi toccare la sua foto e tenere giù il dito per qualche istante; allo stesso modo puoi riportarlo allo stato originale. Per evitare accuse accidentali, un singolo tocco non accuserà il sospetto di omicidio!</translation>
    </message>
    <message>
        <source>__HOWTOPLAY4__</source>
        <translation type="obsolete">Per accusare l&apos;assassino usa il bottone &quot;Risolvi il caso&quot;. Quando la modalità accusa è attiva, un singolo tocco sulla foto di un sospetto produrrà un&apos;accusa nei suoi confronti. Se invece cambi idea, e vuoi continuare a indagare, usa il tasto &quot;Annulla&quot;, e il gioco tornerà alla modalità investigazione.</translation>
    </message>
    <message>
        <source>__GOODLUCK__</source>
        <translation type="obsolete">Buona fortuna e buon divertimento!</translation>
    </message>
    <message>
        <source>__BLACKWICKED__</source>
        <translation type="obsolete">Edvin Rab
Sviluppatore</translation>
    </message>
    <message>
        <source>__BLACKFAIRY__</source>
        <translation type="obsolete">Karolina Kalocsai
Traduzione serba</translation>
    </message>
    <message>
        <source>__DRU__</source>
        <translation type="obsolete">Dru Moore
Traduzione inglese</translation>
    </message>
    <message>
        <source>__ALESSANDRO__</source>
        <translation type="obsolete">Alessandro Pra&apos;
Traduzione italiana</translation>
    </message>
    <message>
        <source>__RESTART__</source>
        <translation type="obsolete">Ricomincia la partita</translation>
    </message>
</context>
</TS>
