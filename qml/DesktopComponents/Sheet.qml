// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt.labs.components.native 1.0

import "../EvidencehuntCore" 1.0

Rectangle {
    /*
    id: welcomeSheet
    anchors.fill: parent
    color: '#ffffff'
    opacity: 0
    z: 1000
    */

    property string title;
    property alias content: contentItem.children;
    property double ratio: 1

    anchors.fill: parent
    color: '#ffffff'
    opacity: 0
    z: 1000

    Rectangle {

        id: sheetMenuBar
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: 68*ratio
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#aaaaaa" }
            GradientStop { position: 0.33; color: "#cccccc" }
            GradientStop { position: 1.0; color: "#888888" }
        }
        z: 1001

        Text {
            font.pixelSize: 26*ratio;
            font.bold: true;
            text: title;
            anchors.top: parent.top;
            anchors.left: parent.left;
            anchors.topMargin: 16*ratio;
            anchors.leftMargin: 16*ratio
        }

        Button {
            id: closeSheetButton
            text: qsTr("__CLOSE__") + emptyString
            anchors.top: parent.top;
            anchors.right: parent.right;
            anchors.topMargin: 16*ratio;
            anchors.rightMargin: 16*ratio
            font.pixelSize: 26*ratio;
            onClicked: {
                parent.parent.close();
            }
        }

    }

    Item {

         id: contentItem

         anchors.top: sheetMenuBar.bottom
         anchors.bottom: parent.bottom
         anchors.left: parent.left
         anchors.right: parent.right

    }

    PropertyAnimation {
        id: sheetAnimation
        property: "opacity"; to: 1; duration: 500
    }

    function open() {
        sheetAnimation.to = 1;
        sheetAnimation.target = this;
        sheetAnimation.start();
    }

    function close() {
        sheetAnimation.to = 0;
        sheetAnimation.target = this;
        sheetAnimation.start();
    }

}
