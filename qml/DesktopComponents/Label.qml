import QtQuick 1.1

Text {

    id: label

    property double ratio: 1

    font.pixelSize: 24*ratio;
    anchors.verticalCenter: parent.verticalCenter;
    anchors.left: parent.left;
    anchors.leftMargin: 20*ratio;

    SystemPalette {id:pal}
}
