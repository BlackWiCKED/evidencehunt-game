import QtQuick 1.1
import com.nokia.meego 1.0
import "EvidencehuntCore" 1.0

Item {

    id: field

    Image { source: "EvidencehuntCore/pics/crimescene.jpg"; anchors.fill: parent; }

    Dialog {
        id: exitDialog
        title:Label   { color:"orange"; text:qsTr("__EXIT__") + emptyString }
        content:Label { color:"white"; text:qsTr("__EXITQUESTION__") + emptyString; }
        buttons: Row {
            Button {id: bOk; text: "OK"; onClicked: Qt.quit(); }
            Button {id: bCancel; text:qsTr("__CANCEL__") + emptyString; onClicked: exitDialog.close()}
        }
    }

    Dialog {
        id: resetDialog
        title:Label { color:"orange"; text:qsTr("__RESET__") + emptyString }
        content:Label { color:"white"; text: qsTr("__RESETQUESTION__") + emptyString; }
        buttons: Row {
            Button {id: bRestartOk; text: "OK"; onClicked: { resetDialog.close(); reset(); } }
            Button {id: bRestartCancel; text: qsTr("__CANCEL__") + emptyString; onClicked: resetDialog.close(); }
        }
    }

    Dialog {
        id: welcomeDialog

        title: Label { color: "orange"; text: qsTr("__ABOUTWELCOME__") + emptyString; }
        content: Text { color:"white"; font.pixelSize: 22; text: qsTr("__ABOUTWELCOMETEXT__") + emptyString; }
        buttons: Column {
            Row {
                Button { text: qsTr("__LANGUAGE__") + emptyString; onClicked: { languageDialog.returnToWelcome = true; welcomeDialog.close(); languageDialog.open(); } }
                Button { text: qsTr("__CONTINUE__") + emptyString; onClicked: { welcomeDialog.close(); welcomeSheet.open(); } }
            }
            Button {anchors.horizontalCenter: parent.horizontalCenter; text: qsTr("__CLOSE__") + emptyString; onClicked: { welcomeDialog.close(); } }
        }
    }

    Sheet {
        id: scoreboardSheet
        acceptButtonText: qsTr("__CLOSE__") + emptyString
        buttons: Item { Text { font.pixelSize: 26; font.bold: true; text: qsTr("__TOPLIST__") + emptyString; anchors.top: parent.top; anchors.left: parent.left; anchors.topMargin: 16; anchors.leftMargin: 16 } }
        z: 1000
        content: Scoreboard {
            id: scoreboardSheetContents
            anchors.fill: parent
        }
        onAccepted: scoreboardSheet.close();
    }



    Sheet {
        id: welcomeSheet
        acceptButtonText: qsTr("__CLOSE__") + emptyString
        buttons: Item { Text { font.pixelSize: 26; font.bold: true; text: "EvidenceHunt Game - " + qsTr("__ABOUT__") + emptyString; anchors.top: parent.top; anchors.left: parent.left; anchors.topMargin: 16; anchors.leftMargin: 16 } }
        z: 1000
        content: Flickable {

            anchors.fill: parent
            anchors.margins: 30

            contentWidth: welcomeSheetContents.width
            contentHeight: welcomeSheetContents.height

            flickableDirection: Flickable.VerticalFlick

            Welcome { id: welcomeSheetContents }
        }

        onAccepted: welcomeSheet.close();
    }

    property list<ListModel> languageSelector;


    languageSelector: [

    ListModel {
        id: languageSelector0
        ListElement { name: "English" }
        ListElement { name: "Hungarian" }
        ListElement { name: "Serbian" }
        ListElement { name: "Italian" }
    },

    ListModel {
        id: languageSelector1
        ListElement { name: "Angol" }
        ListElement { name: "Magyar" }
        ListElement { name: "Szerb" }
        ListElement { name: "Olasz" }
    },

    ListModel {
        id: languageSelector2
        ListElement { name: "Engleski" }
        ListElement { name: "Mađarski" }
        ListElement { name: "Srpski" }
        ListElement { name: "Italijanski" }
    },

    ListModel {
        id: languageSelector3
        ListElement { name: "Inglese" }
        ListElement { name: "Ungherese" }
        ListElement { name: "Serbo" }
        ListElement { name: "Italiano" }
    }
    ]

    SelectionDialog {

        property bool returnToWelcome: false;

        id: languageDialog
        titleText: qsTr("__LANGUAGE__") + emptyString
        model: languageSelector[selectedLanguage]
        selectedIndex: selectedLanguage
        onAccepted:  {

            selectLanguage(languageDialog.selectedIndex);

            if (returnToWelcome) {
                returnToWelcome = false;
                welcomeDialog.open();
            } else {
                //settingsDialog.open();
            }

        }
    }

    Dialog {
        id: shareDialog
        title:Label { color:"orange"; text:qsTr("__SHARE__") + emptyString }
        content: Column {
            spacing: 10

            Text {
                width:parent.width
                height: 90
                color:"white"; font.pixelSize: 22;
                text: qsTr("__SHAREINFO__") + emptyString;
            }

                Rectangle {
                    height: 64
                    color: "transparent"
                    width:parent.width
                    Image { id: shareFacebookIcon; source: "EvidencehuntCore/icons/facebook.png"; }
                    Text {
                        id: shareFacebookText;
                        anchors.left: parent.left;
                        anchors.leftMargin: 64;
                        anchors.top: parent.top;
                        anchors.topMargin: 16;
                        color:"#3B5998";
                        font.pixelSize: 22;
                        font.underline: true;
                        text: qsTr("__OFFICIALFACEBOOK__") + emptyString;
                    }
                    MouseArea {
                        anchors.top: shareFacebookIcon.top
                        anchors.bottom: shareFacebookIcon.bottom
                        anchors.left: shareFacebookIcon.left
                        anchors.right: shareFacebookText.right
                        onClicked: Qt.openUrlExternally("http://www.facebook.com/pages/EvidenceHunt-Game/129432933818266");

                    }
                }

                Rectangle {
                    height: 84
                    color: "transparent"
                    width:parent.width;
                    Image { id: shareTwitterIcon; source: "EvidencehuntCore/icons/twitter.png"; }
                    Text {
                        id: shareTwitterText;
                        anchors.left: parent.left;
                        anchors.leftMargin: 64;
                        anchors.top: parent.top;
                        anchors.topMargin: 16;
                        color:"#4099FF";
                        font.pixelSize: 22;
                        font.underline: true;
                        text: qsTr("__FOLLOWTWITTER__") + emptyString;
                    }
                    MouseArea {
                        anchors.top: shareTwitterIcon.top
                        anchors.bottom: shareTwitterIcon.bottom
                        anchors.left: shareTwitterIcon.left
                        anchors.right: shareTwitterText.right
                        onClicked: Qt.openUrlExternally("http://www.twitter.com/BlackWiCKED/");
                    }
                }

        }
        buttons: Row {
            Button {id: bShareContinue; text: qsTr("__SHARE__") + emptyString; onClicked: shareGame(); }
            Button {id: bShareCancel; text: qsTr("__CLOSE__") + emptyString; onClicked: shareDialog.close(); }
        }

    }


    Grid {
        id: evidenceTiles
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 20
        columns: 8; spacing: 1

        Repeater {
            id: repeater
            model: tiles
            delegate: Tile {}
        }
    }

    EvidenceInfo {

        id: evidenceInfoInstance
        opacity:  0

        state: isInfoVisible ? 'visible' : ''

        states: State {
            name: "visible"
            PropertyChanges { target: evidenceInfoInstance; opacity: 1 }
        }

        transitions: Transition {
            PropertyAnimation { properties: "opacity"; easing.type: Easing.InOutQuad }
        }

    }

    Button {
        id: onlineScoreboardButton
        text: qsTr("__SUBMITPERSONALBEST__") + emptyString
        anchors.bottom: evidenceInfoInstance.bottom
        anchors.bottomMargin: 20
        anchors.horizontalCenter: evidenceInfoInstance.horizontalCenter
        visible: maxScore > maxScoreSubmitted && !isPlaying && !hasWon && isInfoVisible
        onClicked: {
            openScoreboard();
            scoreboardSheet.open();
        }
    }

    GameMenuTop {
        id: gamemenu
    }

    Rectangle {

        id: expandableMenu

        property bool isExpanded: true;
        color: "transparent"
        anchors.top: gamemenu.top
        anchors.topMargin: 80
        state: "collapsed-landscape" //!isExpanded ? ((field.width < field.height) ? 'collapsed-portrait':'collapsed-landscape') : ((field.width < field.height) ? 'expanded-portrait':'expanded-landscape');

        states: [
            State {
                name: "collapsed-portrait";
                AnchorChanges { target: expandableMenu; anchors.bottom: field.bottom; anchors.left: field.left; anchors.top: undefined; anchors.right: undefined; }
                PropertyChanges { target: expandableMenu; width:field.width; height: (field.height >= 900) ? 390 : (field.height - 510); }
            },
            State {
                name: "collapsed-landscape";
                AnchorChanges { target: expandableMenu; anchors.top: field.top; anchors.right: field.right; anchors.bottom: undefined; anchors.left: undefined;}
                PropertyChanges { target: expandableMenu; width: (field.width >= 900) ? 390 : (field.width - 510); height: field.height;}
            },
            State {
                name: "expanded-portrait";
                AnchorChanges { target: expandableMenu; anchors.bottom: field.bottom; anchors.left: field.left; anchors.top: undefined; anchors.right: undefined; }
                PropertyChanges { target: expandableMenu; width:field.width; height: 390; }
            },
            State {
                name: "expanded-landscape";
                AnchorChanges { target: expandableMenu; anchors.top: field.top; anchors.right: field.right; anchors.bottom: undefined; anchors.left: undefined;}
                PropertyChanges { target: expandableMenu; width: 390; height: field.height;}
            }
        ]

        SuspectList {
            id: suspectlist
        }

        Score {
            id: scoreboard
        }

        GameMenuBottom {
            id: gameMenuBottom
        }

    }

    function openWelcomeDialog() {
        welcomeDialog.open();
    }

    function scoreBoardLoaded() {
        scoreboardSheetContents.hasNoError();
    }

    function openBoardErrorDialog() {
        scoreboardSheetContents.hasError();
    }

}

