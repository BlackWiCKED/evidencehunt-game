<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="sr_CS">
<context>
    <name>EvidencehuntGame</name>
    <message>
        <location filename="../EvidenceHunt.cpp" line="174"/>
        <source>__SHOCKINGMURDER__</source>
        <translation>Strašno ubistvo</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="175"/>
        <source>__MURDERWEAPON__</source>
        <translation>Smrtonosno oružje</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="176"/>
        <source>__ELIMINATIONCLUE__</source>
        <translation>Oslobađajući dokaz</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="177"/>
        <source>__INCRIMINATINGCLUE__</source>
        <translation>Opterećujući dokaz</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="178"/>
        <source>__HIGHPROFILE__</source>
        <translation>Detaljan opis</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="179"/>
        <source>__AIRTIGHTALIBI__</source>
        <translation>Čvrst alibi</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="180"/>
        <source>__SEARCHWARRANT__</source>
        <translation>Dozvola za pretres kuce</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="181"/>
        <source>__SUSPECTINFO__</source>
        <translation>Informacije o osumnjičenom</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="182"/>
        <source>__LARGEREWARD__</source>
        <translation>Nagrada za informaciju</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="203"/>
        <source>__SUSPECTNUMBER__</source>
        <translation>Imaš %1 osumnjičenih</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="210"/>
        <source>__FOUNDMURDERWEAPON__</source>
        <translation>Našao si smrtonosno oružju</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="210"/>
        <location filename="../EvidenceHunt.cpp" line="238"/>
        <source>__YOURREWARD__</source>
        <translation>Tvoja nagrada je $%1!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="214"/>
        <location filename="../EvidenceHunt.cpp" line="218"/>
        <source>__KILLER__</source>
        <translation>Ubica</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="222"/>
        <source>__DOUBLEREWARD__</source>
        <translation>Tvoji prihodi za nađene dokaze su duplirani!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="226"/>
        <source>__HASALIBI__</source>
        <translation>Otkrio si da je %1 nevin, i ima čvrst alibi</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="230"/>
        <source>__EXTRATIME__</source>
        <translation>Dobio si +5 vremena da rešiš slučaj!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="238"/>
        <source>__FOUNDLARGEREWARD__</source>
        <translation>Našao si veliku nagradu!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="570"/>
        <source>__KEEPTHISWAY__</source>
        <translation>Samo tako nastavi</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="571"/>
        <source>__CONGRATS__</source>
        <translation>Čestitamo</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="571"/>
        <source>__FOUNDTHEMURDERER__</source>
        <translation>Uspešno si otkrio ubicu!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="582"/>
        <source>__NEWPERSONALBEST__</source>
        <translation>Novi personalni rekord</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="584"/>
        <source>__TERRIBLEMISTAKE</source>
        <translation>Katastrofalna greška</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="584"/>
        <source>__MISSEDTHEMURDERER__</source>
        <translation>Osumnjičio si pogrešnu osobu!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="649"/>
        <source>__TIMEISUP__</source>
        <translation>Vreme je isteklo</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="649"/>
        <source>__TIMETOSOLVE__</source>
        <translation>Vreme je da rešiš slučaj!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="711"/>
        <source>__PLAYING__</source>
        <translation>Igram EvidenceHunt Game na moj Nokia N9/N950!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="726"/>
        <source>__PLAYER__</source>
        <translation>Igrač</translation>
    </message>
</context>
<context>
    <name>GameMenuBottom</name>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="27"/>
        <source>__TOPLIST__</source>
        <translation>Toplista</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="56"/>
        <source>__RESTART__</source>
        <translation>Restart</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="56"/>
        <source>__NEXTCASE__</source>
        <translation>Sledeći slučaj</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="82"/>
        <source>__CANCEL__</source>
        <translation>Odustani</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="82"/>
        <source>__SOLVE__</source>
        <translation>Reši slučaj</translation>
    </message>
</context>
<context>
    <name>GameMenuTop</name>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="30"/>
        <source>__SHARE__</source>
        <translation>Podeli</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="57"/>
        <source>__SETTINGS__</source>
        <translation>Podešavanja</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="82"/>
        <source>__ABOUT__</source>
        <translation>Vodič</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="107"/>
        <source>__QUIT__</source>
        <translation>Izlaz</translation>
    </message>
</context>
<context>
    <name>Score</name>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Pare</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Score.qml" line="20"/>
        <source>__SCORE__</source>
        <translation>Poeni</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Score.qml" line="31"/>
        <source>__TIME__</source>
        <translation>Dani</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Score.qml" line="48"/>
        <source>__FOUND__</source>
        <translation>Nađeno</translation>
    </message>
</context>
<context>
    <name>Scoreboard</name>
    <message>
        <source>__RESET__</source>
        <translation type="obsolete">Reset</translation>
    </message>
    <message>
        <source>__RESETQUESTION__</source>
        <translation type="obsolete">Sigurno želiš da resetuješ aktuelnu igru?</translation>
    </message>
    <message>
        <source>__CANCEL__</source>
        <translation type="obsolete">Odustani</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="25"/>
        <source>__TOPALL__</source>
        <translation>Svih vremena</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="43"/>
        <source>__TOPMONTH__</source>
        <translation>Ovog meseca</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="62"/>
        <source>__TOPWEEK__</source>
        <translation>Ove nedelje</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="81"/>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="212"/>
        <source>__LOADING__</source>
        <translation>Učitavanje rezultata...</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="88"/>
        <source>__RETRY__</source>
        <translation>Ponovo</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="112"/>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="119"/>
        <source>__LASTSCORE__</source>
        <translation>Trenutni rezultat</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="125"/>
        <source>__BESTSCORE__</source>
        <translation>Najbolji rezultat</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="131"/>
        <source>__WINNINGSTREAK__</source>
        <translation>Pobednička serija</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="137"/>
        <source>__FINISHFIRST__</source>
        <translation>Prvo završi trenutnu igru!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="143"/>
        <source>__WASSUBMITTED__</source>
        <translation>Tvoj najbolji rezultat je poslat!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="172"/>
        <source>__SUBMIT__</source>
        <translation>Pošalji</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="197"/>
        <source>__ONLINE__</source>
        <translation>Onlajn toplista</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="228"/>
        <source>__CONNECTIONERROR__</source>
        <translation>Toplista je nedostupna.</translation>
    </message>
</context>
<context>
    <name>Sheet</name>
    <message>
        <source>__ABOUT__</source>
        <translation type="obsolete">Vodič</translation>
    </message>
    <message>
        <location filename="DesktopComponents/Sheet.qml" line="50"/>
        <source>__CLOSE__</source>
        <translation>Zatvori</translation>
    </message>
</context>
<context>
    <name>Suspect</name>
    <message>
        <location filename="EvidencehuntCore/Suspect.qml" line="83"/>
        <source>__SUSPECT__</source>
        <translation>Osumnjičeni</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="12"/>
        <source>__GAMEGOALTITLE__</source>
        <translation>Cilj igre</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="20"/>
        <source>__GAMEGOALTEXT__</source>
        <translation>EvidenceHunt je logistička igra. Glavni junak, u ovom slučaju, Ti, je detektiv koji treba da reši misterično ubistvo. Imaš pomoć i razne tragove da uspešno rešiš slučaj. Svaka misterija ima čistu logističku pozadinu, naravno možeš i da pogađaš. Ali pazi, greška te može koštati karijere!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="27"/>
        <source>__GAMEPLAYTITLE__</source>
        <translation>Igranje igre</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="37"/>
        <source>__GAMEPLAY1__</source>
        <translation>Na početku svake igre, igrač se upoznaje se sa detaljima ubistva. Dobija listu osumnjičenih, i lokaciju zločina. Zahvaljujući svom iskustvu, neke lokacije se otkriju već na početku istraživanja, znači tamo se sigurno ne kriju dokazi, ali daju pomoć pri otkrivanju okolnih dokaza.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="47"/>
        <source>__GAMEPLAY2__</source>
        <translation>Za ispravno rešenje u svakom slučaju neophodno je, da nedužne osunjičene isključimo iz kruga  mogućih osumnjićenih.U pametno odigranoj igri, na kraju ostaje samo jedan osumnjičeni.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="59"/>
        <source>__GAMEPLAY3__</source>
        <translation>Detektiv ima ograničeno vreme za rešavanje igre. Za jedan dan, samo jednu lokaciju možemo da istražimo, igra broji preostale dane. Ako nam ponestane vremena, od raspolagajućih dokaza moramo da predpostavimo ko je ubica. Ovde možemo da se oslonimo na sreću, ali da se ova situacija ne desi,  potrudimo se pametno istraživati i naći svaki dokaz.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="69"/>
        <source>__GAMEPLAY4__</source>
        <translation>Igrač samo jednu šansu ima za osumnjičenje. Ako donese pogrešnu odluku, to znači kraj igre. U suprotnom, ne treba tugovati, uvek možemo pokrenuti novu igru.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="78"/>
        <source>__GAMEINTERFACETITLE__</source>
        <translation>Interfejs igre</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="98"/>
        <source>__GAMEINTERFACETEXT__</source>
        <translation>Lokacije istraživanja u igri,predstavlja jedna 8x8 set slagalica. Neki delovi slagalice su već na početku igre otkrivene. Svaki element, krije jednu brojku u sebi., ova brojka pokazuje broj okolnih dokaza. Različite boje, predstavljaju različita svojstva:

Belo – Ne istražena lokacija
Crveno –Lokacija zločina i smrtonosno oružje
Lila – Korisna informacija ili nagrada prilikom istrage
Žuto – Informacija o osumnjičenom
Plavo – Izuzimajući dokaz u vezi ubice
Zeleno –Dokaz na štetu ubice 
Crno – Lokacija bez dokaza</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="105"/>
        <source>__HOWTOPLAYTITLE__</source>
        <translation>Način igre</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="116"/>
        <source>__HOWTOPLAY1__</source>
        <translation>Na lokacijama istrage imamo dve mogućnosti. Ako želimo pretražiti datu lokaciju, kliknimo kratko na željeni deo slagalice. U suprotnom ako želimo isključiti lokaciju, potreban je duži klik. U ovom zadjem slučaju, jedan crveni X ukazuje na isključenje, što isto tako sa - dugim klikom - možemo odrešiti X-a.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="125"/>
        <source>__HOWTOPLAY2__</source>
        <translation>Pregledati ponovo već istražene lokacije ne košta nas još jedan dan. Detektiv ovde u stvari, proverava  dokumentaciju već pretražene lokacije, i nema potrebe za ponovno istraživanje.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="134"/>
        <source>__HOWTOPLAY3__</source>
        <translation>Lista osumnjičenih radi slično. Prema otkrivenom dokazu, sa dugim klikom možemo isključiti pojedine         osobe, koje posle možemo vratiti u krug osumnjičenih isto tako sa dugim klikom. Da izbegnemo slučajno osumnjičenje, kratki klik, ne znači trenutni odabir.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="153"/>
        <source>__HOWTOPLAY4__</source>
        <translation>Za pravo osumnjičenje koristimo “Reši slučaj” dugme, sa kojim aktiviramo opciju osumnjičenja. Kada je opcija aktivna, klikom na sliku osumnjičenog, možemo da izaberemo ubicu. Ukoliko smo se predomislili, i ipak želimo da istražujemo dalje, koristimo “Odustani” dugme i igra se vraća u prvobitno stanje.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="163"/>
        <source>__GOODLUCK__</source>
        <translation>Želimo ti puno sreće i dobru zabavu!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="184"/>
        <source>__BLACKWICKED__</source>
        <translation>Edvin Rab
Programiranje</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="201"/>
        <source>__BLACKFAIRY__</source>
        <translation>Karolina Kaločai
Srpski prevod</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="217"/>
        <source>__DRU__</source>
        <translation>Dru Moore
Engleski prevod</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="233"/>
        <source>__ALESSANDRO__</source>
        <translation>Alessandro Pra&apos;
Italjinanski prevod</translation>
    </message>
</context>
<context>
    <name>blackberry</name>
    <message>
        <location filename="blackberry.qml" line="16"/>
        <source>__EXIT__</source>
        <translation>Izlaz</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="26"/>
        <source>__EXITQUESTION__</source>
        <translation>Sigurno želiš da napustiš EvidenceHunt Game igru?</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="35"/>
        <location filename="blackberry.qml" line="60"/>
        <source>__CANCEL__</source>
        <translation>Odustani</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="41"/>
        <source>__RESET__</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="51"/>
        <source>__RESETQUESTION__</source>
        <translation>Sigurno želiš da resetuješ aktuelnu igru?</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="66"/>
        <source>__ABOUTWELCOME__</source>
        <translation>Pozdravljam te u EvidenceHunt igri!</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="74"/>
        <source>__ABOUTWELCOMETEXT__</source>
        <translation>
Na sledećih nekoliko strana možeš da se upoznaš sa pravilima igre, i dobiješ opis za efikasnu i uspešnu igru.

Pre nego što počnemo, molim te izaberi jezik igre.

Trenutno, možeš da biraš od 4 jezika.
Engleski, Madjarski, Srpski i Italijanski.
</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="81"/>
        <location filename="blackberry.qml" line="154"/>
        <source>__LANGUAGE__</source>
        <translation>Biranje jezika</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="82"/>
        <source>__CONTINUE__</source>
        <translation>Dalje</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="84"/>
        <location filename="blackberry.qml" line="247"/>
        <source>__CLOSE__</source>
        <translation>Zatvori</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="90"/>
        <source>__TOPLIST__</source>
        <translation>Toplista</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="99"/>
        <source>__ABOUT__</source>
        <translation>Vodič</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="173"/>
        <source>__SHARE__</source>
        <translation>Podeli</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="187"/>
        <source>__SHAREINFO__</source>
        <translation>Molim te podeli igru EvidenceHunt Game sa priljateljima na poznatim društvenim mrežama. Hvala najlepše!
Ako ti se dopada igra, molim te pridruži nam se na našoj Fejsbuk Stranici!</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="204"/>
        <source>__OFFICIALFACEBOOK__</source>
        <translation>Zvanićna Fejsbuk Stranica</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="230"/>
        <source>__FOLLOWTWITTER__</source>
        <translation>Prati Kreatora Na Twitteru</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="288"/>
        <source>__SUBMITPERSONALBEST__</source>
        <translation>Pošalji tvoj najbolji rezultat</translation>
    </message>
</context>
<context>
    <name>desktop</name>
    <message>
        <source>__SETTINGS__</source>
        <translation type="obsolete">Podešavanja</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="98"/>
        <source>__ABOUT__</source>
        <translation>Vodič</translation>
    </message>
    <message>
        <source>__QUIT__</source>
        <translation type="obsolete">Izlaz</translation>
    </message>
    <message>
        <source>__FINDMURDERER__</source>
        <translation type="obsolete">Nađi ubicu</translation>
    </message>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Pare</translation>
    </message>
    <message>
        <source>__TIME__</source>
        <translation type="obsolete">Vreme</translation>
    </message>
    <message>
        <source>__SOLVE__</source>
        <translation type="obsolete">Reši slučaj</translation>
    </message>
    <message>
        <source>__RESTART__</source>
        <translation type="obsolete">Restart igre</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="15"/>
        <source>__EXIT__</source>
        <translation>Izlaz</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="25"/>
        <source>__EXITQUESTION__</source>
        <translation>Sigurno želiš da napustiš EvidenceHunt Game igru?</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="34"/>
        <location filename="desktop.qml" line="59"/>
        <source>__CANCEL__</source>
        <translation>Odustani</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="40"/>
        <source>__RESET__</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="50"/>
        <source>__RESETQUESTION__</source>
        <translation>Sigurno želiš da resetuješ aktuelnu igru?</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="65"/>
        <source>__ABOUTWELCOME__</source>
        <translation>Pozdravljam te u EvidenceHunt igri!</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="73"/>
        <source>__ABOUTWELCOMETEXT__</source>
        <translation>
Na sledećih nekoliko strana možeš da se upoznaš sa pravilima igre, i dobiješ opis za efikasnu i uspešnu igru.

Pre nego što počnemo, molim te izaberi jezik igre.

Trenutno, možeš da biraš od 4 jezika.
Engleski, Madjarski, Srpski i Italijanski.
</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="80"/>
        <location filename="desktop.qml" line="153"/>
        <source>__LANGUAGE__</source>
        <translation>Biranje jezika</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="81"/>
        <source>__CONTINUE__</source>
        <translation>Dalje</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="83"/>
        <location filename="desktop.qml" line="246"/>
        <source>__CLOSE__</source>
        <translation>Zatvori</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="89"/>
        <source>__TOPLIST__</source>
        <translation>Toplista</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="172"/>
        <source>__SHARE__</source>
        <translation>Podeli</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="186"/>
        <source>__SHAREINFO__</source>
        <translation>Molim te podeli igru EvidenceHunt Game sa priljateljima na poznatim društvenim mrežama. Hvala najlepše!
Ako ti se dopada igra, pridruži nam se na Fejsbuku!</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="203"/>
        <source>__OFFICIALFACEBOOK__</source>
        <translation>Zvanićna Fejsbuk Stranica</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="229"/>
        <source>__FOLLOWTWITTER__</source>
        <translation>Prati Kreatora Na Twitteru</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="286"/>
        <source>__SUBMITPERSONALBEST__</source>
        <translation>Pošalji tvoj najbolji rezultat</translation>
    </message>
</context>
<context>
    <name>harmattan</name>
    <message>
        <location filename="harmattan.qml" line="142"/>
        <location filename="harmattan.qml" line="206"/>
        <source>__SHARE__</source>
        <translation>Podeli</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="150"/>
        <source>__SHAREINFO__</source>
        <translation>Molim te podeli igru EvidenceHunt Game sa priljateljima na poznatim
društvenim mrežama. Hvala najlepše!
Ako ti se dopada igra, molim te pridruži nam se na našoj Fejsbuk Stranici!</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="167"/>
        <source>__OFFICIALFACEBOOK__</source>
        <translation>Zvanićna Fejsbuk Stranica</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="193"/>
        <source>__FOLLOWTWITTER__</source>
        <translation>Prati Kreatora Na Twitteru</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="41"/>
        <location filename="harmattan.qml" line="47"/>
        <location filename="harmattan.qml" line="61"/>
        <location filename="harmattan.qml" line="207"/>
        <source>__CLOSE__</source>
        <translation>Zatvori</translation>
    </message>
    <message>
        <source>__SETTINGS__</source>
        <translation type="obsolete">Podešavanja</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="62"/>
        <source>__ABOUT__</source>
        <translation>Vodič</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="38"/>
        <location filename="harmattan.qml" line="123"/>
        <source>__LANGUAGE__</source>
        <translation>Biranje jezika</translation>
    </message>
    <message>
        <source>__ENGLISH__</source>
        <translation type="obsolete">Engleski</translation>
    </message>
    <message>
        <source>__HUNGARIAN__</source>
        <translation type="obsolete">Mađarski</translation>
    </message>
    <message>
        <source>__SERBIAN__</source>
        <translation type="obsolete">Srpski</translation>
    </message>
    <message>
        <source>__QUIT__</source>
        <translation type="obsolete">Izlaz</translation>
    </message>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Pare</translation>
    </message>
    <message>
        <source>__TIME__</source>
        <translation type="obsolete">Dani</translation>
    </message>
    <message>
        <source>__FOUND__</source>
        <translation type="obsolete">Nađeno</translation>
    </message>
    <message>
        <source>__RESTART__</source>
        <translation type="obsolete">Restart igre</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="17"/>
        <location filename="harmattan.qml" line="27"/>
        <source>__CANCEL__</source>
        <translation>Odustani</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="13"/>
        <source>__EXIT__</source>
        <translation>Izlaz</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="14"/>
        <source>__EXITQUESTION__</source>
        <translation>Sigurno želiš da napustiš EvidenceHunt Game igru?</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="23"/>
        <source>__RESET__</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="24"/>
        <source>__RESETQUESTION__</source>
        <translation>Sigurno želiš da resetuješ aktuelnu igru?</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="34"/>
        <source>__ABOUTWELCOME__</source>
        <translation>Pozdravljam te u EvidenceHunt igri!</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="35"/>
        <source>__ABOUTWELCOMETEXT__</source>
        <translation>
Na sledećih nekoliko strana možeš da se upoznaš sa pravilima igre, i
dobiješ opis za efikasnu i uspešnu igru.

Pre nego što počnemo, molim te izaberi jezik igre.

Trenutno, možeš da biraš od 4 jezika.
Engleski, Madjarski, Srpski i Italijanski.
</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="39"/>
        <source>__CONTINUE__</source>
        <translation>Dalje</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="48"/>
        <source>__TOPLIST__</source>
        <translation>Toplista</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="248"/>
        <source>__SUBMITPERSONALBEST__</source>
        <translation>Pošalji tvoj najbolji rezultat</translation>
    </message>
    <message>
        <source>__GAMEGOALTITLE__</source>
        <translation type="obsolete">Cilj igre</translation>
    </message>
    <message>
        <source>__GAMEGOALTEXT__</source>
        <translation type="obsolete">EvidenceHunt je logistička igra. Glavni junak, u ovom slučaju, Ti, je detektiv koji treba da reši misterično ubistvo. Imaš pomoć i razne tragove da uspešno rešiš slučaj. Svaka misterija ima čistu logističku pozadinu, naravno možeš i da pogađaš. Ali pazi, greška te može koštati karijere!</translation>
    </message>
    <message>
        <source>__GAMEPLAYTITLE__</source>
        <translation type="obsolete">Igranje igre</translation>
    </message>
    <message>
        <source>__GAMEPLAY1__</source>
        <translation type="obsolete">Na početku svake igre, igrač se upoznaje se sa detaljima ubistva. Dobija listu osumnjičenih, i lokaciju zločina. Zahvaljujući svom iskustvu, neke lokacije se otkriju već na početku istraživanja, znači tamo se sigurno ne kriju dokazi, ali daju pomoć pri otkrivanju okolnih dokaza.</translation>
    </message>
    <message>
        <source>__GAMEPLAY2__</source>
        <translation type="obsolete">Za ispravno rešenje u svakom slučaju neophodno je, da nedužne osunjičene isključimo iz kruga  mogućih osumnjićenih.U pametno odigranoj igri, na kraju ostaje samo jedan osumnjičeni.</translation>
    </message>
    <message>
        <source>__GAMEPLAY3__</source>
        <translation type="obsolete">Detektiv ima ograničeno vreme za rešavanje igre. Za jedan dan, samo jednu lokaciju možemo da istražimo, igra broji preostale dane. Ako nam ponestane vremena, od raspolagajućih dokaza moramo da predpostavimo ko je ubica. Ovde možemo da se oslonimo na sreću, ali da se ova situacija ne desi,  potrudimo se pametno istraživati i naći svaki dokaz.</translation>
    </message>
    <message>
        <source>__GAMEPLAY4__</source>
        <translation type="obsolete">Igrač samo jednu šansu ima za osumnjičenje. Ako donese pogrešnu odluku, to znači kraj igre. U suprotnom, ne treba tugovati, uvek možemo pokrenuti novu igru.</translation>
    </message>
    <message>
        <source>__GAMEINTERFACETITLE__</source>
        <translation type="obsolete">Interfejs igre</translation>
    </message>
    <message>
        <source>__GAMEINTERFACETEXT__</source>
        <translation type="obsolete">Lokacije istraživanja u igri,predstavlja jedna 8x8 set slagalica. Neki delovi slagalice su već na početku igre otkrivene. Svaki element, krije jednu brojku u sebi., ova brojka pokazuje broj okolnih dokaza. Različite boje, predstavljaju različita svojstva:

Belo – Ne istražena lokacija
Crveno –Lokacija zločina i smrtonosno oružje
Lila – Korisna informacija ili nagrada prilikom istrage
Žuto – Informacija o osumnjičenom
Plavo – Izuzimajući dokaz u vezi ubice
Zeleno –Dokaz na štetu ubice 
Crno – Lokacija bez dokaza</translation>
    </message>
    <message>
        <source>__HOWTOPLAYTITLE__</source>
        <translation type="obsolete">Način igre</translation>
    </message>
    <message>
        <source>__HOWTOPLAY1__</source>
        <translation type="obsolete">Na lokacijama istrage imamo dve mogućnosti. Ako želimo pretražiti datu lokaciju, kliknimo kratko na željeni deo slagalice. U suprotnom ako želimo isključiti lokaciju, potreban je duži klik. U ovom zadjem slučaju, jedan crveni X ukazuje na isključenje, što isto tako sa - dugim klikom - možemo odrešiti X-a.</translation>
    </message>
    <message>
        <source>__HOWTOPLAY2__</source>
        <translation type="obsolete">Pregledati ponovo već istražene lokacije ne košta nas još jedan dan. Detektiv ovde u stvari, proverava  dokumentaciju već pretražene lokacije, i nema potrebe za ponovno istraživanje.</translation>
    </message>
    <message>
        <source>__HOWTOPLAY3__</source>
        <translation type="obsolete">Lista osumnjičenih radi slično. Prema otkrivenom dokazu, sa dugim klikom možemo isključiti pojedine         osobe, koje posle možemo vratiti u krug osumnjičenih isto tako sa dugim klikom. Da izbegnemo slučajno osumnjičenje, kratki klik, ne znači trenutni odabir.</translation>
    </message>
    <message>
        <source>__HOWTOPLAY4__</source>
        <translation type="obsolete">Za pravo osumnjičenje koristimo “Reši slučaj” dugme, sa kojim aktiviramo opciju osumnjičenja. Kada je opcija aktivna, klikom na sliku osumnjičenog, možemo da izaberemo ubicu. Ukoliko smo se predomislili, i ipak želimo da istražujemo dalje, koristimo “Odustani” dugme i igra se vraća u prvobitno stanje.</translation>
    </message>
    <message>
        <source>__GOODLUCK__</source>
        <translation type="obsolete">Želimo ti puno sreće i dobru zabavu!</translation>
    </message>
    <message>
        <source>__BLACKWICKED__</source>
        <translation type="obsolete">Edvin Rab
Programiranje</translation>
    </message>
    <message>
        <source>__BLACKFAIRY__</source>
        <translation type="obsolete">Karolina Kaločai
Srpski prevod</translation>
    </message>
    <message>
        <source>__DRU__</source>
        <translation type="obsolete">Dru Moore
Engleski prevod</translation>
    </message>
    <message>
        <source>__ALESSANDRO__</source>
        <translation type="obsolete">Alessandro Pra&apos;
Italjinanski prevod</translation>
    </message>
    <message>
        <source>__SOLVE__</source>
        <translation type="obsolete">Reši slučaj</translation>
    </message>
</context>
</TS>
