<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="hu_HU">
<context>
    <name>EvidencehuntGame</name>
    <message>
        <location filename="../EvidenceHunt.cpp" line="174"/>
        <source>__SHOCKINGMURDER__</source>
        <translation>Borzalmas gyilkosság</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="175"/>
        <source>__MURDERWEAPON__</source>
        <translation>Gyilkos fegyver</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="176"/>
        <source>__ELIMINATIONCLUE__</source>
        <translation>Felmentő bizonyíték</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="177"/>
        <source>__INCRIMINATINGCLUE__</source>
        <translation>Terhelő bizonyíték</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="178"/>
        <source>__HIGHPROFILE__</source>
        <translation>Részletes személyleírás</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="179"/>
        <source>__AIRTIGHTALIBI__</source>
        <translation>Sziklaszilárd alibi</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="180"/>
        <source>__SEARCHWARRANT__</source>
        <translation>Házkutatási engedély</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="181"/>
        <source>__SUSPECTINFO__</source>
        <translation>Információ a gyanúsítottról</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="182"/>
        <source>__LARGEREWARD__</source>
        <translation>Nyomravezetői díj</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="203"/>
        <source>__SUSPECTNUMBER__</source>
        <translation>%1 gyanúsítottad van</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="210"/>
        <source>__FOUNDMURDERWEAPON__</source>
        <translation>Megtaláltad a gyilkos fegyvert</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="210"/>
        <location filename="../EvidenceHunt.cpp" line="238"/>
        <source>__YOURREWARD__</source>
        <translation>A jutalmad $%1!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="214"/>
        <location filename="../EvidenceHunt.cpp" line="218"/>
        <source>__KILLER__</source>
        <translation>A gyilkos</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="222"/>
        <source>__DOUBLEREWARD__</source>
        <translation>Bizonyítékok találatakor a bevételeid megduplázódnak!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="226"/>
        <source>__HASALIBI__</source>
        <translation>Felfedezted, hogy %1 ártatlan, és sziklasziráld alibije van</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="230"/>
        <source>__EXTRATIME__</source>
        <translation>Az ügy megoldására +5 időegységed van!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="238"/>
        <source>__FOUNDLARGEREWARD__</source>
        <translation>Nagy jutalomra leltél!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="570"/>
        <source>__KEEPTHISWAY__</source>
        <translation>Csak így tovább</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="571"/>
        <source>__CONGRATS__</source>
        <translation>Gratulálunk</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="571"/>
        <source>__FOUNDTHEMURDERER__</source>
        <translation>Sikeresen felfedted a gyilkos kilétét!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="582"/>
        <source>__NEWPERSONALBEST__</source>
        <translation>Új egyéni csúcs</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="584"/>
        <source>__TERRIBLEMISTAKE</source>
        <translation>Szörnyű baklövés</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="584"/>
        <source>__MISSEDTHEMURDERER__</source>
        <translation>Rossz személyt gyanúsítottál meg!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="649"/>
        <source>__TIMEISUP__</source>
        <translation>Az idő lejárt</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="649"/>
        <source>__TIMETOSOLVE__</source>
        <translation>Eljött az idő az ügy megoldására!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="711"/>
        <source>__PLAYING__</source>
        <translation>Éppen EvidenceHunt Játékot játszok a Nokia N9/N950 telefonomon!</translation>
    </message>
    <message>
        <location filename="../EvidenceHunt.cpp" line="726"/>
        <source>__PLAYER__</source>
        <translation>Játékos</translation>
    </message>
</context>
<context>
    <name>GameMenuBottom</name>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="27"/>
        <source>__TOPLIST__</source>
        <translation>Toplista</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="56"/>
        <source>__RESTART__</source>
        <translation>Újraindítás</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="56"/>
        <source>__NEXTCASE__</source>
        <translation>Következő ügy</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="82"/>
        <source>__CANCEL__</source>
        <translation>Mégsem</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuBottom.qml" line="82"/>
        <source>__SOLVE__</source>
        <translation>Az ügy megoldása</translation>
    </message>
</context>
<context>
    <name>GameMenuTop</name>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="30"/>
        <source>__SHARE__</source>
        <translation>Megosztás</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="57"/>
        <source>__SETTINGS__</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="82"/>
        <source>__ABOUT__</source>
        <translation>Ismertető</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/GameMenuTop.qml" line="107"/>
        <source>__QUIT__</source>
        <translation>Kilépés</translation>
    </message>
</context>
<context>
    <name>Score</name>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Pénz</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Score.qml" line="20"/>
        <source>__SCORE__</source>
        <translation>Pont</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Score.qml" line="31"/>
        <source>__TIME__</source>
        <translation>Napok</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Score.qml" line="48"/>
        <source>__FOUND__</source>
        <translation>Találat</translation>
    </message>
</context>
<context>
    <name>Scoreboard</name>
    <message>
        <source>__RESET__</source>
        <translation type="obsolete">Újraindítás</translation>
    </message>
    <message>
        <source>__RESETQUESTION__</source>
        <translation type="obsolete">Biztosan újra szeretnéd inditítani az aktuális játékot?</translation>
    </message>
    <message>
        <source>__CANCEL__</source>
        <translation type="obsolete">Mégsem</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="25"/>
        <source>__TOPALL__</source>
        <translation>Mindenkori</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="43"/>
        <source>__TOPMONTH__</source>
        <translation>Ebben a hónapban</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="62"/>
        <source>__TOPWEEK__</source>
        <translation>Ezen a héten</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="81"/>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="212"/>
        <source>__LOADING__</source>
        <translation>Az eredmények betöltése...</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="88"/>
        <source>__RETRY__</source>
        <translation>Újra</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="112"/>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="119"/>
        <source>__LASTSCORE__</source>
        <translation>Aktuális eredmény</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="125"/>
        <source>__BESTSCORE__</source>
        <translation>Legjobb eredmény</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="131"/>
        <source>__WINNINGSTREAK__</source>
        <translation>Nyerő széria</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="137"/>
        <source>__FINISHFIRST__</source>
        <translation>Előbb fejezd be az aktuális játékot!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="143"/>
        <source>__WASSUBMITTED__</source>
        <translation>A legjobb eredményedet beküldted!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="172"/>
        <source>__SUBMIT__</source>
        <translation>Beküldés</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="197"/>
        <source>__ONLINE__</source>
        <translation>Online toplista</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Scoreboard.qml" line="228"/>
        <source>__CONNECTIONERROR__</source>
        <translation>A toplista nem elérhető.</translation>
    </message>
</context>
<context>
    <name>Sheet</name>
    <message>
        <source>__ABOUT__</source>
        <translation type="obsolete">Ismertető</translation>
    </message>
    <message>
        <location filename="DesktopComponents/Sheet.qml" line="50"/>
        <source>__CLOSE__</source>
        <translation>Bezár</translation>
    </message>
</context>
<context>
    <name>Suspect</name>
    <message>
        <location filename="EvidencehuntCore/Suspect.qml" line="83"/>
        <source>__SUSPECT__</source>
        <translation>Gyanúsított</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="12"/>
        <source>__GAMEGOALTITLE__</source>
        <translation>A játék célja</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="20"/>
        <source>__GAMEGOALTEXT__</source>
        <translation>Az EvidenceHunt egy logikai szerepjáték. A főhős, jelen esetben Te, egy nyomozót alakít, aki gyilkossági rejtélyeket kell, hogy megoldjon. Számos segítség és nyom áll rendelkezésedre ahhoz, hogy munkádat sikeresen elvégezhesd. Minden rejtélynek van tisztán logikailag levezethető megoldása, persze találgatni is lehet. De vigyázz, a tévedés a karriered végét jelentheti!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="27"/>
        <source>__GAMEPLAYTITLE__</source>
        <translation>A játék menete</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="37"/>
        <source>__GAMEPLAY1__</source>
        <translation>Minden játék kezdeténél a gyilkosság részleteivel ismerkedik meg a játékos. Megkapja a gyanúsítottak listáját, és a bűntény helyszínét. Tapasztalatának köszönhetően néhány helyszín már rögtön a nyomozás kezdeténél felfedi magát, hiszen azok biztosan nem relytenek bizonyítékokat, ellenben segítséget nyújtanak a környező bizonyítékok felfedéséhez.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="47"/>
        <source>__GAMEPLAY2__</source>
        <translation>A helyes megoldáshoz mindenképpen elengedhetetlen, hogy az ártatlan gyanúsítottakat kizárjuk a lehetséges elkövetők köréből. Ügyes játék esetén, a végén csak egyetlen gyanúsított marad.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="59"/>
        <source>__GAMEPLAY3__</source>
        <translation>A játék elvégzéséhez meghatározott mennyiségű idő áll a nyomozó rendelkezésére. Egy napon csak egyetlen helyszínt kutathatunk át, a játék a hátralévő napokat számolja. Amennyiben kifutunk az időből, a rendelkezésre álló bizonyítékokból kell következtetnünk a gyilkos személyére. Itt szerepe lehet a szerencsének is, de hogy ez a helyzet ne fordulhasson elő, törekedjünk az ésszerű nyomozásra és minden bizonyíték megtalálására.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="69"/>
        <source>__GAMEPLAY4__</source>
        <translation>A játékos csak egyetlen gyanúsítási lehetőséggel élhet. Amennyiben téved, az a játék azonnali végét jelenti. Szomorkodni azonban nem kell, hiszen bármikor új játékot indíthatunk.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="78"/>
        <source>__GAMEINTERFACETITLE__</source>
        <translation>A játék felülete</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="98"/>
        <source>__GAMEINTERFACETEXT__</source>
        <translation>A nyomozás helyszíneit a játékban egy 8x8-as kirakós készlet jelképezi. A kirakó elemei közül néhány már a játék kezdetekor felfedésre került. Minden elem egy számot rejt magában, ez a szám mutatja az elem közvetlen közelében lévő bizonyítékok számát. A különböző színek különböző tulajdonsággal bírnak:

Fehér – Fel nem kutatott helyszín
Piros – A bűntett helyszíne és a gyilkos fegyver
Lila – Hasznos információ vagy nyeremény a nyomozás során
Sárga – Információ a gyanúsítotról
Kék – Kizáró bizonyíték a gyilkos személyére nézve
Zöld – A gyilkos számára terhelő bizonyíték
Fekete – Bizonyíték nélküli helyszín</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="105"/>
        <source>__HOWTOPLAYTITLE__</source>
        <translation>A játék kezelése</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="116"/>
        <source>__HOWTOPLAY1__</source>
        <translation>A nyomozás helyszínein két lehetőségünk van. Ha át szeretnénk kutatni az adott helyszínt, röviden kattintsunk a kívánt kirakós elemre. Amennyiben viszont kizárni szeretnénk a helyszínt, hosszú kattintás szükséges. Ez utóbbi esetben egy piros X jelöli a kizárás tényét, amit hasonlóképpen – hosszú kattintással – oldhatunk fel.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="125"/>
        <source>__HOWTOPLAY2__</source>
        <translation>A már átkutatott helyszínek újbóli megvizsgálása nem vesz igénybe újabb napot. A nyomozó itt valójában a helyszíni szemle dokumentációját olvassa újra, és nincs szükség újbóli kutatásra.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="134"/>
        <source>__HOWTOPLAY3__</source>
        <translation>A gyanúsítottak listája hasonlóképpen működik. A felfedett bizonyítékok alapján hosszú kattintással kizárhatunk egyes személyeket, melyeket később akár újra fel is vehetünk a lehetséges elkövetők körébe - szintén hosszú kattintással. A véletlen meggyanúsítások elkerülése végett a rövid kattintás nem jelent azonnali kiválasztást.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="153"/>
        <source>__HOWTOPLAY4__</source>
        <translation>A valódi gyanúsításhoz használjuk “Az ügy megoldása” gombot, mellyel aktiváljuk a gyanúsítás módot. Aktív gyanúsítás mód esetén a gyanúsított arcképére történő egyetlen kattintásal választhatjuk ki a gyilkost. Amennyiben meggondoltuk magunkat, és inkább tovább szeretnénk nyomozni, használjuk a “Mégsem” gombot, és a játék visszaáll eredeti állapotába.</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="163"/>
        <source>__GOODLUCK__</source>
        <translation>Sok sikert és jó szórakozást kívánunk!</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="184"/>
        <source>__BLACKWICKED__</source>
        <translation>Rab Edvin
Programozó</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="201"/>
        <source>__BLACKFAIRY__</source>
        <translation>Kalocsai Karolina
Szerb fordítás</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="217"/>
        <source>__DRU__</source>
        <translation>Dru Moore
Angol fordítás</translation>
    </message>
    <message>
        <location filename="EvidencehuntCore/Welcome.qml" line="233"/>
        <source>__ALESSANDRO__</source>
        <translation>Alessandro Pra&apos;
Olasz fordítás</translation>
    </message>
</context>
<context>
    <name>blackberry</name>
    <message>
        <location filename="blackberry.qml" line="16"/>
        <source>__EXIT__</source>
        <translation>Kilépés</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="26"/>
        <source>__EXITQUESTION__</source>
        <translation>Biztosan el akarod hagyni az EvidenceHunt Game játékot?</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="35"/>
        <location filename="blackberry.qml" line="60"/>
        <source>__CANCEL__</source>
        <translation>Mégsem</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="41"/>
        <source>__RESET__</source>
        <translation>Újraindítás</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="51"/>
        <source>__RESETQUESTION__</source>
        <translation>Biztosan újra szeretnéd inditítani az aktuális játékot?</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="66"/>
        <source>__ABOUTWELCOME__</source>
        <translation>Üdvözöllek az EvidenceHunt játékban!</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="74"/>
        <source>__ABOUTWELCOMETEXT__</source>
        <translation>
A következő néhány oldalon megismerkedhetsz a játékszabályokkal, és útmutatást kapsz a hatékony és eredményes játékhoz.

Mielőtt belevágnánk, kérlek válaszd ki a játék nyelvét.

Jelenleg 4 nyelv közül választhatsz:
Angol, Magyar, Szerb és Olasz.
</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="81"/>
        <location filename="blackberry.qml" line="154"/>
        <source>__LANGUAGE__</source>
        <translation>Nyelv választás</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="82"/>
        <source>__CONTINUE__</source>
        <translation>Tovább</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="84"/>
        <location filename="blackberry.qml" line="247"/>
        <source>__CLOSE__</source>
        <translation>Bezár</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="90"/>
        <source>__TOPLIST__</source>
        <translation>Toplista</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="99"/>
        <source>__ABOUT__</source>
        <translation>Ismertető</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="173"/>
        <source>__SHARE__</source>
        <translation>Megosztás</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="187"/>
        <source>__SHAREINFO__</source>
        <translation>Kérlek oszd meg az EvidenceHunt Game-t barátaiddal az ismert közösségi oldalakon. Nagyon szépen köszönöm!
Ha tetszik a játék, kérlek csatlakozz Facebook Csoportunkhoz!</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="204"/>
        <source>__OFFICIALFACEBOOK__</source>
        <translation>Hivatalos Facebook Oldal</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="230"/>
        <source>__FOLLOWTWITTER__</source>
        <translation>Kövesd A Készítőt A Twitteren</translation>
    </message>
    <message>
        <location filename="blackberry.qml" line="288"/>
        <source>__SUBMITPERSONALBEST__</source>
        <translation>Eredmény beküldése</translation>
    </message>
</context>
<context>
    <name>desktop</name>
    <message>
        <source>__SETTINGS__</source>
        <translation type="obsolete">Beállítások</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="98"/>
        <source>__ABOUT__</source>
        <translation>Ismertető</translation>
    </message>
    <message>
        <source>__QUIT__</source>
        <translation type="obsolete">Kilépés</translation>
    </message>
    <message>
        <source>__FINDMURDERER__</source>
        <translation type="obsolete">Találd meg a gyilkost</translation>
    </message>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Pénz</translation>
    </message>
    <message>
        <source>__TIME__</source>
        <translation type="obsolete">Idő</translation>
    </message>
    <message>
        <source>__SOLVE__</source>
        <translation type="obsolete">Az ügy megoldása</translation>
    </message>
    <message>
        <source>__RESTART__</source>
        <translation type="obsolete">Játék újraindítása</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="15"/>
        <source>__EXIT__</source>
        <translation>Kilépés</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="25"/>
        <source>__EXITQUESTION__</source>
        <translation>Biztosan el akarod hagyni az EvidenceHunt Game játékot?</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="34"/>
        <location filename="desktop.qml" line="59"/>
        <source>__CANCEL__</source>
        <translation>Mégsem</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="40"/>
        <source>__RESET__</source>
        <translation>Újraindítás</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="50"/>
        <source>__RESETQUESTION__</source>
        <translation>Biztosan újra szeretnéd inditítani az aktuális játékot?</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="65"/>
        <source>__ABOUTWELCOME__</source>
        <translation>Üdvözöllek az EvidenceHunt játékban!</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="73"/>
        <source>__ABOUTWELCOMETEXT__</source>
        <translation>
A következő néhány oldalon megismerkedhetsz a játékszabályokkal, és útmutatást kapsz a hatékony és eredményes játékhoz.

Mielőtt belevágnánk, kérlek válaszd ki a játék nyelvét.

Jelenleg 4 nyelv közül választhatsz:
Angol, Magyar, Szerb és Olasz.
</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="80"/>
        <location filename="desktop.qml" line="153"/>
        <source>__LANGUAGE__</source>
        <translation>Nyelv választás</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="81"/>
        <source>__CONTINUE__</source>
        <translation>Tovább</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="83"/>
        <location filename="desktop.qml" line="246"/>
        <source>__CLOSE__</source>
        <translation>Bezár</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="89"/>
        <source>__TOPLIST__</source>
        <translation>Toplista</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="172"/>
        <source>__SHARE__</source>
        <translation>Megosztás</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="186"/>
        <source>__SHAREINFO__</source>
        <translation>Kérlek oszd meg az EvidenceHunt Game-t barátaiddal az ismert közösségi oldalakon. Nagyon szépen köszönöm!
Ha tetszik a játék, csatlakozz Facebook Csoportunkhoz!</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="203"/>
        <source>__OFFICIALFACEBOOK__</source>
        <translation>Hivatalos Facebook Oldal</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="229"/>
        <source>__FOLLOWTWITTER__</source>
        <translation>Kövesd A Készítőt A Twitteren</translation>
    </message>
    <message>
        <location filename="desktop.qml" line="286"/>
        <source>__SUBMITPERSONALBEST__</source>
        <translation>Eredmény beküldése</translation>
    </message>
</context>
<context>
    <name>evidencehunt</name>
    <message>
        <source>__SETTINGS__</source>
        <oldsource>Settings</oldsource>
        <translation type="obsolete">Beállítások</translation>
    </message>
    <message>
        <source>__ABOUT__</source>
        <oldsource>About</oldsource>
        <translation type="obsolete">Ismertető</translation>
    </message>
    <message>
        <source>__QUIT__</source>
        <oldsource>Quit</oldsource>
        <translation type="obsolete">Kilépés</translation>
    </message>
    <message>
        <source>__FINDMURDERER__</source>
        <oldsource>Find the murderer</oldsource>
        <translation type="obsolete">Találd meg a gyilkost</translation>
    </message>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Pénz</translation>
    </message>
    <message>
        <source>Score</source>
        <translation type="obsolete">Eredmény</translation>
    </message>
    <message>
        <source>__TIME__</source>
        <oldsource>Time</oldsource>
        <translation type="obsolete">Idő</translation>
    </message>
    <message>
        <source>__SOLVE__</source>
        <translation type="obsolete">Az ügy megoldása</translation>
    </message>
    <message>
        <source>__RESTART__</source>
        <oldsource>Restart the game!</oldsource>
        <translation type="obsolete">Játék újraindítása</translation>
    </message>
</context>
<context>
    <name>harmattan</name>
    <message>
        <source>__SETTINGS__</source>
        <translation type="obsolete">Beállítások</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="62"/>
        <source>__ABOUT__</source>
        <translation>Ismertető</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="38"/>
        <location filename="harmattan.qml" line="123"/>
        <source>__LANGUAGE__</source>
        <translation>Nyelv választás</translation>
    </message>
    <message>
        <source>__ENGLISH__</source>
        <translation type="obsolete">Angol</translation>
    </message>
    <message>
        <source>__HUNGARIAN__</source>
        <translation type="obsolete">Magyar</translation>
    </message>
    <message>
        <source>__SERBIAN__</source>
        <translation type="obsolete">Szerb</translation>
    </message>
    <message>
        <source>__QUIT__</source>
        <translation type="obsolete">Kilépés</translation>
    </message>
    <message>
        <source>__FINDMURDERER__</source>
        <translation type="obsolete">Találd meg a gyilkost</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="142"/>
        <location filename="harmattan.qml" line="206"/>
        <source>__SHARE__</source>
        <translation>Megosztás</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="150"/>
        <source>__SHAREINFO__</source>
        <translation>Kérlek oszd meg az EvidenceHunt Game-t barátaiddal az ismert
közösségi oldalakon. Nagyon szépen köszönöm!
Ha tetszik a játék, kérlek csatlakozz Facebook Csoportunkhoz!</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="167"/>
        <source>__OFFICIALFACEBOOK__</source>
        <translation>Hivatalos Facebook Oldal</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="193"/>
        <source>__FOLLOWTWITTER__</source>
        <translation>Kövesd A Készítőt A Twitteren</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="41"/>
        <location filename="harmattan.qml" line="47"/>
        <location filename="harmattan.qml" line="61"/>
        <location filename="harmattan.qml" line="207"/>
        <source>__CLOSE__</source>
        <translation>Bezár</translation>
    </message>
    <message>
        <source>__MONEY__</source>
        <translation type="obsolete">Pénz</translation>
    </message>
    <message>
        <source>__TIME__</source>
        <translation type="obsolete">Napok</translation>
    </message>
    <message>
        <source>__FOUND__</source>
        <translation type="obsolete">Találat</translation>
    </message>
    <message>
        <source>__SOLVE__</source>
        <translation type="obsolete">Az ügy megoldása</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="17"/>
        <location filename="harmattan.qml" line="27"/>
        <source>__CANCEL__</source>
        <translation>Mégsem</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="13"/>
        <source>__EXIT__</source>
        <translation>Kilépés</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="14"/>
        <source>__EXITQUESTION__</source>
        <translation>Biztosan el akarod hagyni az EvidenceHunt Game játékot?</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="23"/>
        <source>__RESET__</source>
        <translation>Újraindítás</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="24"/>
        <source>__RESETQUESTION__</source>
        <translation>Biztosan újra szeretnéd inditítani az aktuális játékot?</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="34"/>
        <source>__ABOUTWELCOME__</source>
        <translation>Üdvözöllek az EvidenceHunt játékban!</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="35"/>
        <source>__ABOUTWELCOMETEXT__</source>
        <translation>
A következő néhány oldalon megismerkedhetsz a játékszabályokkal, és
útmutatást kapsz a hatékony és eredményes játékhoz.

Mielőtt belevágnánk, kérlek válaszd ki a játék nyelvét.

Jelenleg 4 nyelv közül választhatsz:
Angol, Magyar, Szerb és Olasz.
</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="39"/>
        <source>__CONTINUE__</source>
        <translation>Tovább</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="48"/>
        <source>__TOPLIST__</source>
        <translation>Toplista</translation>
    </message>
    <message>
        <location filename="harmattan.qml" line="248"/>
        <source>__SUBMITPERSONALBEST__</source>
        <translation>Eredmény beküldése</translation>
    </message>
    <message>
        <source>__GAMEGOALTITLE__</source>
        <translation type="obsolete">A játék célja</translation>
    </message>
    <message>
        <source>__GAMEGOALTEXT__</source>
        <translation type="obsolete">Az EvidenceHunt egy logikai szerepjáték. A főhős, jelen esetben Te, egy nyomozót alakít, aki gyilkossági rejtélyeket kell, hogy megoldjon. Számos segítség és nyom áll rendelkezésedre ahhoz, hogy munkádat sikeresen elvégezhesd. Minden rejtélynek van tisztán logikailag levezethető megoldása, persze találgatni is lehet. De vigyázz, a tévedés a karriered végét jelentheti!</translation>
    </message>
    <message>
        <source>__GAMEPLAYTITLE__</source>
        <translation type="obsolete">A játék menete</translation>
    </message>
    <message>
        <source>__GAMEPLAY1__</source>
        <translation type="obsolete">Minden játék kezdeténél a gyilkosság részleteivel ismerkedik meg a játékos. Megkapja a gyanúsítottak listáját, és a bűntény helyszínét. Tapasztalatának köszönhetően néhány helyszín már rögtön a nyomozás kezdeténél felfedi magát, hiszen azok biztosan nem relytenek bizonyítékokat, ellenben segítséget nyújtanak a környező bizonyítékok felfedéséhez.</translation>
    </message>
    <message>
        <source>__GAMEPLAY2__</source>
        <translation type="obsolete">A helyes megoldáshoz mindenképpen elengedhetetlen, hogy az ártatlan gyanúsítottakat kizárjuk a lehetséges elkövetők köréből. Ügyes játék esetén, a végén csak egyetlen gyanúsított marad.</translation>
    </message>
    <message>
        <source>__GAMEPLAY3__</source>
        <translation type="obsolete">A játék elvégzéséhez meghatározott mennyiségű idő áll a nyomozó rendelkezésére. Egy napon csak egyetlen helyszínt kutathatunk át, a játék a hátralévő napokat számolja. Amennyiben kifutunk az időből, a rendelkezésre álló bizonyítékokból kell következtetnünk a gyilkos személyére. Itt szerepe lehet a szerencsének is, de hogy ez a helyzet ne fordulhasson elő, törekedjünk az ésszerű nyomozásra és minden bizonyíték megtalálására.</translation>
    </message>
    <message>
        <source>__GAMEPLAY4__</source>
        <translation type="obsolete">A játékos csak egyetlen gyanúsítási lehetőséggel élhet. Amennyiben téved, az a játék azonnali végét jelenti. Szomorkodni azonban nem kell, hiszen bármikor új játékot indíthatunk.</translation>
    </message>
    <message>
        <source>__GAMEINTERFACETITLE__</source>
        <translation type="obsolete">A játék felülete</translation>
    </message>
    <message>
        <source>__GAMEINTERFACETEXT__</source>
        <translation type="obsolete">A nyomozás helyszíneit a játékban egy 8x8-as kirakós készlet jelképezi. A kirakó elemei közül néhány már a játék kezdetekor felfedésre került. Minden elem egy számot rejt magában, ez a szám mutatja az elem közvetlen közelében lévő bizonyítékok számát. A különböző színek különböző tulajdonsággal bírnak:

Fehér – Fel nem kutatott helyszín
Piros – A bűntett helyszíne és a gyilkos fegyver
Lila – Hasznos információ vagy nyeremény a nyomozás során
Sárga – Információ a gyanúsítotról
Kék – Kizáró bizonyíték a gyilkos személyére nézve
Zöld – A gyilkos számára terhelő bizonyíték
Fekete – Bizonyíték nélküli helyszín</translation>
    </message>
    <message>
        <source>__HOWTOPLAYTITLE__</source>
        <translation type="obsolete">A játék kezelése</translation>
    </message>
    <message>
        <source>__HOWTOPLAY1__</source>
        <translation type="obsolete">A nyomozás helyszínein két lehetőségünk van. Ha át szeretnénk kutatni az adott helyszínt, röviden kattintsunk a kívánt kirakós elemre. Amennyiben viszont kizárni szeretnénk a helyszínt, hosszú kattintás szükséges. Ez utóbbi esetben egy piros X jelöli a kizárás tényét, amit hasonlóképpen – hosszú kattintással – oldhatunk fel.</translation>
    </message>
    <message>
        <source>__HOWTOPLAY2__</source>
        <translation type="obsolete">A már átkutatott helyszínek újbóli megvizsgálása nem vesz igénybe újabb napot. A nyomozó itt valójában a helyszíni szemle dokumentációját olvassa újra, és nincs szükség újbóli kutatásra.</translation>
    </message>
    <message>
        <source>__HOWTOPLAY3__</source>
        <translation type="obsolete">A gyanúsítottak listája hasonlóképpen működik. A felfedett bizonyítékok alapján hosszú kattintással kizárhatunk egyes személyeket, melyeket később akár újra fel is vehetünk a lehetséges elkövetők körébe - szintén hosszú kattintással. A véletlen meggyanúsítások elkerülése végett a rövid kattintás nem jelent azonnali kiválasztást.</translation>
    </message>
    <message>
        <source>__HOWTOPLAY4__</source>
        <translation type="obsolete">A valódi gyanúsításhoz használjuk “Az ügy megoldása” gombot, mellyel aktiváljuk a gyanúsítás módot. Aktív gyanúsítás mód esetén a gyanúsított arcképére történő egyetlen kattintásal választhatjuk ki a gyilkost. Amennyiben meggondoltuk magunkat, és inkább tovább szeretnénk nyomozni, használjuk a “Mégsem” gombot, és a játék visszaáll eredeti állapotába.</translation>
    </message>
    <message>
        <source>__GOODLUCK__</source>
        <translation type="obsolete">Sok sikert és jó szórakozást kívánunk!</translation>
    </message>
    <message>
        <source>__BLACKWICKED__</source>
        <translation type="obsolete">Rab Edvin
Programozó</translation>
    </message>
    <message>
        <source>__BLACKFAIRY__</source>
        <translation type="obsolete">Kalocsai Karolina
Szerb fordítás</translation>
    </message>
    <message>
        <source>__DRU__</source>
        <translation type="obsolete">Dru Moore
Angol fordítás</translation>
    </message>
    <message>
        <source>__ALESSANDRO__</source>
        <translation type="obsolete">Alessandro Pra&apos;
Olasz fordítás</translation>
    </message>
    <message>
        <source>__RESTART__</source>
        <translation type="obsolete">Játék újraindítása</translation>
    </message>
</context>
</TS>
