import QtQuick 1.1
import Qt.labs.components.native 1.0

import "DesktopComponents" 1.0
import "EvidencehuntCore" 1.0

Rectangle {

    id: field
    anchors.fill: parent

    Image { source: "EvidencehuntCore/pics/crimescene.jpg"; anchors.fill: parent; }

    Dialog {
        id: exitDialog
        title:Label { font.pixelSize: 32; color:"orange"; text:qsTr("__EXIT__") + emptyString }
        content: Text {
            color:"white";
            width: parent.width-40
            anchors.top: parent.top;
            anchors.topMargin: 20
            anchors.left: parent.left
            anchors.leftMargin: 20;
            font.pixelSize: 28;
            height: 98
            wrapMode: Text.WordWrap
            text:qsTr("__EXITQUESTION__") + emptyString;
        }
        buttons: Row {
            spacing: 20
            height: 98
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.horizontalCenter: parent.horizontalCenter;
            Button { font.pixelSize: 32; id: bOk; anchors.bottom: parent.bottom; anchors.bottomMargin: 20; text: "OK"; onClicked: Qt.quit(); }
            Button { font.pixelSize: 32; id: bCancel; anchors.bottom: parent.bottom; anchors.bottomMargin: 20; text:qsTr("__CANCEL__") + emptyString; onClicked: exitDialog.close()}
        }
    }

    Dialog {
        id: resetDialog
        title:Label { font.pixelSize: 32; color:"orange"; text:qsTr("__RESET__") + emptyString }
        content:Text {
            color:"white";
            width: parent.width-40
            anchors.top: parent.top;
            anchors.topMargin: 20
            anchors.left: parent.left
            anchors.leftMargin: 20;
            font.pixelSize: 28;
            height: 98
            wrapMode: Text.WordWrap
            text: qsTr("__RESETQUESTION__") + emptyString;
        }
        buttons: Row {
            spacing: 20
            height: 98
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.horizontalCenter: parent.horizontalCenter;
            Button {font.pixelSize: 32; id: bRestartOk; anchors.bottom: parent.bottom; anchors.bottomMargin: 20; text: "OK"; onClicked: { resetDialog.close(); reset(); } }
            Button {font.pixelSize: 32; id: bRestartCancel; anchors.bottom: parent.bottom; anchors.bottomMargin: 20; text: qsTr("__CANCEL__") + emptyString; onClicked: resetDialog.close(); }
        }
    }

    Dialog {
        id: welcomeDialog
        title: Label { font.pixelSize: 32; color: "orange"; text: qsTr("__ABOUTWELCOME__") + emptyString; }
        content: Text {
            color:"white";
            width: parent.width-40
            anchors.left: parent.left
            anchors.leftMargin: 20;
            font.pixelSize: 28;
            wrapMode: Text.WordWrap
            text: qsTr("__ABOUTWELCOMETEXT__") + emptyString;
        }
        buttons: Column {
            width: parent.width-40
            height: 160
            spacing: 40
            Row {
                anchors.horizontalCenter: parent.horizontalCenter;
                spacing: 40
                Button { font.pixelSize: 32; text: qsTr("__LANGUAGE__") + emptyString; onClicked: { languageDialog.returnToWelcome = true; welcomeDialog.close(); languageDialog.open(); } }
                Button { font.pixelSize: 32; text: qsTr("__CONTINUE__") + emptyString; onClicked: { welcomeDialog.close(); welcomeSheet.open(); } }
            }
            Button {font.pixelSize: 32; anchors.horizontalCenter: parent.horizontalCenter; text: qsTr("__CLOSE__") + emptyString; onClicked: { welcomeDialog.close(); } }
        }
    }

    Sheet {
        id: scoreboardSheet
        ratio:1.4
        title: qsTr("__TOPLIST__") + emptyString
        content: Scoreboard {
            id: scoreboardSheetContents
            anchors.fill: parent
        }
    }

    Sheet {
        id: welcomeSheet
        ratio:1.4
        title: "EvidenceHunt Game - " + qsTr("__ABOUT__") + emptyString
        content: Flickable {
            anchors.fill: parent
            anchors.margins: 30
            contentWidth: welcomeSheetContents.width
            contentHeight: welcomeSheetContents.height
            flickableDirection: Flickable.VerticalFlick
            Welcome {
                id: welcomeSheetContents
            }
        }
    }

    property list<ListModel> languageSelector;

    languageSelector: [

    ListModel {
        id: languageSelector0
        ListElement { name: "English" }
        ListElement { name: "Hungarian" }
        ListElement { name: "Serbian" }
        ListElement { name: "Italian" }
    },

    ListModel {
        id: languageSelector1
        ListElement { name: "Angol" }
        ListElement { name: "Magyar" }
        ListElement { name: "Szerb" }
        ListElement { name: "Olasz" }
    },

    ListModel {
        id: languageSelector2
        ListElement { name: "Engleski" }
        ListElement { name: "Mađarski" }
        ListElement { name: "Srpski" }
        ListElement { name: "Italijanski" }
    },

    ListModel {
        id: languageSelector3
        ListElement { name: "Inglese" }
        ListElement { name: "Ungherese" }
        ListElement { name: "Serbo" }
        ListElement { name: "Italiano" }
    }
    ]

    SelectionDialog {

        property bool returnToWelcome: false;

        id: languageDialog
        titleText: qsTr("__LANGUAGE__") + emptyString
        model: languageSelector[selectedLanguage]
        selectedIndex: selectedLanguage
        onAccepted:  {

            selectLanguage(languageDialog.selectedIndex);

            if (returnToWelcome) {
                returnToWelcome = false;
                welcomeDialog.open();
            } else {
                //settingsDialog.open();
            }

        }
    }

    Dialog {
        id: shareDialog
        title: Label { color:"orange"; font.pixelSize: 32; text:qsTr("__SHARE__") + emptyString }
        content: Column {
            spacing: 10
                width: parent.width-40
                anchors.top: parent.top
                anchors.topMargin: 20
                anchors.left: parent.left
                anchors.leftMargin: 20

                Text {
                    width:parent.width
                    wrapMode: Text.WordWrap
                    color:"white"; font.pixelSize: 28;
                    text: qsTr("__SHAREINFO__") + emptyString;
                }

                Rectangle {
                    height: 64
                    color: "transparent"
                    width:parent.width
                    Image { id: shareFacebookIcon; source: "EvidencehuntCore/icons/facebook.png"; }
                    Text {
                        id: shareFacebookText;
                        anchors.left: parent.left;
                        anchors.leftMargin: 64;
                        anchors.top: parent.top;
                        anchors.topMargin: 16;
                        color:"#3B5998";
                        font.pixelSize: 32;
                        font.underline: true;
                        text: qsTr("__OFFICIALFACEBOOK__") + emptyString;
                    }
                    MouseArea {
                        anchors.top: shareFacebookIcon.top
                        anchors.bottom: shareFacebookIcon.bottom
                        anchors.left: shareFacebookIcon.left
                        anchors.right: shareFacebookText.right
                        onClicked: Qt.openUrlExternally("http://www.facebook.com/pages/EvidenceHunt-Game/129432933818266");

                    }
                }

                Rectangle {
                    height: 84
                    color: "transparent"
                    width:parent.width;
                    Image { id: shareTwitterIcon; source: "EvidencehuntCore/icons/twitter.png"; }
                    Text {
                        id: shareTwitterText;
                        anchors.left: parent.left;
                        anchors.leftMargin: 64;
                        anchors.top: parent.top;
                        anchors.topMargin: 16;
                        color:"#4099FF";
                        font.pixelSize: 32;
                        font.underline: true;
                        text: qsTr("__FOLLOWTWITTER__") + emptyString;
                    }
                    MouseArea {
                        anchors.top: shareTwitterIcon.top
                        anchors.bottom: shareTwitterIcon.bottom
                        anchors.left: shareTwitterIcon.left
                        anchors.right: shareTwitterText.right
                        onClicked: Qt.openUrlExternally("http://www.twitter.com/BlackWiCKED/");
                    }
                }

        }
        buttons: Row {
            height: 84
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.horizontalCenter: parent.horizontalCenter;
            Button {id: bShareCancel; font.pixelSize: 32; anchors.bottom: parent.bottom; anchors.bottomMargin: 20; text: qsTr("__CLOSE__") + emptyString; onClicked: shareDialog.close(); }
        }

    }

    Item {

        id: wrapper
        width: parent.width
        height: parent.height
        anchors.centerIn: parent

        Grid {
            id: evidenceTiles
            anchors.left: parent.left
            anchors.leftMargin: 80
            anchors.top: parent.top
            anchors.topMargin: 80
            columns: 8; spacing: 1
            Repeater {
                id: repeater
                model: tiles
                delegate: Tile {ratio: 1}
            }
        }

        EvidenceInfo {
            id: evidenceInfoInstance
            opacity:  0
            ratio: 1.4
            state: isInfoVisible ? 'visible' : ''
            states: State {
                name: "visible"
                PropertyChanges { target: evidenceInfoInstance; opacity: 1 }
            }
            /*
            transitions: Transition {
                PropertyAnimation { properties: "opacity"; easing.type: Easing.InOutQuad }
            }
            */
        }

        Button {
            id: onlineScoreboardButton
            text: qsTr("__SUBMITPERSONALBEST__") + emptyString
            anchors.bottom: evidenceInfoInstance.bottom
            anchors.bottomMargin: 20
            anchors.horizontalCenter: evidenceInfoInstance.horizontalCenter
            visible: maxScore > maxScoreSubmitted && !isPlaying && !hasWon && isInfoVisible
            onClicked: {
                openScoreboard();
                scoreboardSheet.open();
            }
        }

        GameMenuTop {
            id: gamemenu
            anchors.topMargin: 60
            anchors.rightMargin: 140
            ratio: 1
        }

        Rectangle {

            id: expandableMenu

            property bool isExpanded: true;
            color: "transparent"

            anchors.top: parent.top
            anchors.topMargin: 160

            state:  (field.width < field.height) ? 'collapsed-portrait':'collapsed-landscape';

            states: [
                State {
                    name: "collapsed-portrait";
                    PropertyChanges { target: evidenceTiles; anchors.topMargin: 155; }
                    PropertyChanges { target: gamemenu; anchors.topMargin: 40; anchors.rightMargin: 220 }
                    PropertyChanges { target: suspectlist; anchors.topMargin: 640; ratio:1.2; anchors.leftMargin: 200 }
                },
                State {
                    name: "collapsed-landscape";
                    AnchorChanges { target: expandableMenu; anchors.top: wrapper.top; anchors.right: wrapper.right; anchors.bottom: undefined; anchors.left: undefined;}
                    PropertyChanges { target: expandableMenu; width: 364*1.4; height: wrapper.height;}
                }
            ]

            SuspectList {
                id: suspectlist
                ratio: 1.4
            }

            Score {
                id: scoreboard
            }

            GameMenuBottom {
                id: gameMenuBottom
                anchors.topMargin: 30
                ratio: 1
            }
        }
    }

    function openWelcomeDialog() {
        welcomeDialog.open();
    }

    function scoreBoardLoaded() {
        scoreboardSheetContents.hasNoError();
    }

    function openBoardErrorDialog() {
        scoreboardSheetContents.hasError();
    }
}

