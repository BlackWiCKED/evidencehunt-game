import QtQuick 1.1
import Qt.labs.particles 1.0

Item {

    property bool explode : false
    property bool repeatable: false

    Particles {
        id: particles
        width: 40
        height: 40
        lifeSpan: 1000
        lifeSpanDeviation: 0
        source: repeatable ? "pics/red-star.png" : "pics/star.png"
        count: 0
        angle: 270
        angleDeviation: 360
        velocity: 100
        velocityDeviation: 20
        z: 100
    }
    states:
        State { name: "exploding"; when: explode
            StateChangeScript { script: particles.burst(200); }
        }

    function callExplode() {
        particles.burst(200);
    }

}

