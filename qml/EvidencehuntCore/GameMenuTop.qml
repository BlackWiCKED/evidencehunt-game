import QtQuick 1.1

Column {

     property double ratio: 1

     anchors.top: parent.top
     anchors.right: parent.right

    Row {

        anchors.right: gamemenu.right
        spacing: 56
        scale: ratio

        Image {
            source: "icons/share.png"
            MouseArea {
                id: shareMouse
                anchors.fill: parent
                onClicked: {
                    shareDialog.open();
                }
            }
            Text {
                font.pixelSize: 19
                font.weight: Font.Bold
                color: "#ffffff"
                style: Text.Outline
                text: qsTr("__SHARE__") + emptyString
                anchors.top: parent.bottom
                anchors.left: parent.left
                anchors.leftMargin: 3
                anchors.topMargin: 12
                width:parent.width-3
                horizontalAlignment: Text.AlignHCenter
            }

        }

        Image {
            source: "icons/wrench_plus_2.png"
            MouseArea {
                id: settingsMouse
                anchors.fill: parent
                onClicked: {
                    evidenceInfo.setVisible(false);
                    //settingsDialog.open();
                    languageDialog.open();
                }
            }
            Text {
                font.pixelSize: 19
                font.weight: Font.Bold
                color: "#ffffff"
                style: Text.Outline
                text: qsTr("__SETTINGS__") + emptyString
                anchors.top: parent.bottom
                anchors.left: parent.left
                anchors.leftMargin: 3
                anchors.topMargin: 12
                width:parent.width-3
                horizontalAlignment: Text.AlignHCenter
            }

        }

        Image {
            source: "icons/info.png"
            MouseArea {
                id: aboutMouse
                anchors.fill: parent
                onClicked: {
                    welcomeDialog.open();
                }
            }
            Text {
                font.pixelSize: 19
                font.weight: Font.Bold
                color: "#ffffff"
                style: Text.Outline
                text: qsTr("__ABOUT__") + emptyString
                anchors.top: parent.bottom
                anchors.left: parent.left
                anchors.leftMargin: 3
                anchors.topMargin: 12
                width:parent.width-3
                horizontalAlignment: Text.AlignHCenter
            }

        }
    }
 }
