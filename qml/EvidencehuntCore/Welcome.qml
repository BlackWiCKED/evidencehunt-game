// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Column {

spacing: 30
width: welcomeSheet.width - 60

Text {
    font.pixelSize: 32;
    font.bold: true;
    text: qsTr("__GAMEGOALTITLE__") + emptyString;

}

Text {
    font.pixelSize: 28;
    wrapMode: Text.Wrap
    width:parent.width
    text: qsTr("__GAMEGOALTEXT__") + emptyString;

}

Text {
    font.pixelSize: 32;
    font.bold: true;
    text: qsTr("__GAMEPLAYTITLE__") + emptyString;
}

Row {
    spacing: 30
    Image { id:welcomeImage2; source: "pics/terrible-murder.jpg"; width:230; height:110; }
    Text {
        font.pixelSize: 28;
        wrapMode: Text.Wrap
        width:welcomeSheet.width - welcomeImage2.width - 60 - 30
        text: qsTr("__GAMEPLAY1__") + emptyString;
    }
}

Row {
    spacing: 30
    Text {
        font.pixelSize: 28;
        wrapMode: Text.Wrap
        width:welcomeSheet.width - welcomeImage3.width - 60 - 30
        text: qsTr("__GAMEPLAY2__") + emptyString;
    }
    Image { id:welcomeImage3; source: "pics/arrest.jpg"; width:230; height:110; }
}

Row {
    spacing: 30
    Image { id:welcomeImage4; source: "pics/time.jpg"; width:230; height:110; }
    Text {
        font.pixelSize: 28;
        wrapMode: Text.Wrap
        width:welcomeSheet.width - welcomeImage4.width - 60 - 30
        text: qsTr("__GAMEPLAY3__") + emptyString;
    }
}

Row {
    spacing: 30
    Text {
        font.pixelSize: 28;
        wrapMode: Text.Wrap
        width:welcomeSheet.width - welcomeImage5.width - 60 - 30
        text: qsTr("__GAMEPLAY4__") + emptyString;
    }
    Image { id:welcomeImage5; source: "pics/fail.jpg"; width:230; height:110; }
}

Text {
    font.pixelSize: 32;
    font.bold: true;
    text: qsTr("__GAMEINTERFACETITLE__") + emptyString;

}

Row {
    spacing: 30
    Column {
        spacing: 30
        Image { id:welcomeImage1; source: "pics/investigation_area.png"; width:230; height:230; }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            Image { source: "tiles/green1.png"; }
            Image { source: "tiles/red2.png"; }
        }
    }

    Text {
        font.pixelSize: 28;
        wrapMode: Text.Wrap
        width:welcomeSheet.width - welcomeImage1.width - 60 - 30
        text: qsTr("__GAMEINTERFACETEXT__") + emptyString;
    }
}

Text {
    font.pixelSize: 32;
    font.bold: true;
    text: qsTr("__HOWTOPLAYTITLE__") + emptyString;

}

Row {
    spacing: 30
    Image { id:welcomeImage6; source: "pics/exclude-tile.png"; }
    Text {
        font.pixelSize: 28;
        wrapMode: Text.Wrap
        width:welcomeSheet.width - welcomeImage6.width - 60 - 30
        text: qsTr("__HOWTOPLAY1__") + emptyString;
    }

}

Text {
    font.pixelSize: 28;
    wrapMode: Text.Wrap
    width:parent.width
    text: qsTr("__HOWTOPLAY2__") + emptyString;
}

Row {
    spacing: 30
    Text {
        font.pixelSize: 28;
        wrapMode: Text.Wrap
        width:welcomeSheet.width - welcomeImage7.width - 60 - 30
        text: qsTr("__HOWTOPLAY3__") + emptyString;
    }
    Image { id:welcomeImage7; source: "pics/exclude-suspect.png"; }

}

Row {
    spacing: 30

    Column {
        spacing:15
        Image { id:welcomeImage8; source: "pics/button-solve.png"; }
        Image { id:welcomeImage9; source: "pics/button-cancel.png"; }
    }

    Text {
        font.pixelSize: 28
        wrapMode: Text.Wrap
        width:welcomeSheet.width - welcomeImage8.width - 60 - 30
        text: qsTr("__HOWTOPLAY4__") + emptyString;
    }


}

Text {
    anchors.horizontalCenter: parent.horizontalCenter
    font.pixelSize: 32
    font.bold: true
    text: qsTr("__GOODLUCK__") + emptyString;
}

Row {
    spacing: 35
    anchors.horizontalCenter: parent.horizontalCenter

    Column {
        spacing: 15
        Image { id:blackwickedImage; source: "crew/blackwicked.png";

            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("http://www.blackwicked.com");
            }

        }
        Text {
            id:blackwickedText
            width:160
            font.pixelSize: 16;
            text: qsTr("__BLACKWICKED__") + emptyString;
        }

    }

    Column {
        spacing: 15
        Image { id:blackfairyImage; source: "crew/blackfairy.png";
            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("http://www.blackfairy-jewelry.com");
            }
        }
        Text {
            id: blackfairyText
            width:160
            font.pixelSize: 16;
            text: qsTr("__BLACKFAIRY__") + emptyString;
        }
    }

    Column {
        spacing: 15
        Image { id:druImage; source: "crew/dru.png";
            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("http://www.dru-id.co.uk");
            }
        }
        Text {
            id: druText
            width:160
            font.pixelSize: 16;
            text: qsTr("__DRU__") + emptyString;
        }
    }

    Column {
        spacing: 15
        Image { id:alessandroImage; source: "crew/alessandro.png";
            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("http://www.craniocreations.com");
            }
        }
        Text {
            id: alessandroText
            width:160
            font.pixelSize: 16;
            text: qsTr("__ALESSANDRO__") + emptyString;
        }
    }

}

Text {
    anchors.horizontalCenter: parent.horizontalCenter
    font.pixelSize: 26;
    color: "#0000ff"
    font.underline: true
    text: "Privacy Policy"
    MouseArea {
        anchors.fill: parent
        onClicked: Qt.openUrlExternally("http://evidencehunt.blackwicked.com/privacy.php");
    }
}

Text {
    anchors.horizontalCenter: parent.horizontalCenter
    font.pixelSize: 22;
    text: "2013 - Subotica, Serbia"
}

}
