// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Grid {

    property double ratio: 1

    anchors.left: parent.left
    anchors.top: parent.top
    anchors.topMargin: 26*ratio
    columns: 3; spacing: 10*ratio

    Repeater {
        id: repeater2
        model: suspects
        delegate: Suspect {ratio: parent.ratio}
    }
}
