import QtQuick 1.1

Flipable {
    id: flipable2
    property int angle: 0
    property double ratio: 1

/*
    function showRequestInfo(text) {
        //log.text = log.text + "\n" + text
        console.log(text)
    }
*/
    width: 98*ratio;  height: 130*ratio
    transform: Rotation { origin.x: 49*ratio; origin.y: 65*ratio; axis.x: 0; axis.y: 1; axis.z: 0; angle: flipable2.angle }
    opacity: 1

    front: Item {

        anchors.bottom: parent.bottom

        width: 98*ratio; height: 130*ratio
        //scale: ratio
        opacity: 1

        Image {
            anchors.fill: parent
            source: "pics/suspect"+modelData.profile+".png";
            opacity: modelData.excluded ? 0.5 : 1
        }

        Text {
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            width: parent.width
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 14*ratio
            text: modelData.roleText; color: "white"; font.bold: true
            opacity: 1
            style: Text.Outline
            wrapMode: Text.WordWrap
        }

        /*
        Image {
            anchors.centerIn: parent
            source: "pics/flag.png"; opacity: modelData.hasFlag
            Behavior on opacity { NumberAnimation {} }
        }
        */

        /*
        Image {
            id: excludeButton
            anchors.top: parent.top
            anchors.topMargin: 3
            anchors.right: parent.right
            anchors.rightMargin: 3
            source: "icons/delete.png"
            opacity: !modelData.excluded
        }
        */

        Image {
            id: excludedIndicator
            anchors.verticalCenter: parent.verticalCenter
            source: "pics/excluded.png"
            width:parent.width
            opacity: modelData.excluded
        }

    }



    back:  Rectangle {
        anchors.centerIn: parent
        width: 98*ratio; height: 130*ratio
        //scale: ratio
        color: "black"

            Image {
        source: "pics/cardback.jpg"; anchors.fill: parent


        Text {
            anchors.centerIn: parent
            text: qsTr("__SUSPECT__") + ": #"+(index+1) + emptyString; color: "white"; font.bold: true
            opacity: 1
            style: Text.Outline
            font.pixelSize: 14*ratio
        }
        /*
        Image {
            anchors.centerIn: parent
            source: "pics/bomb.png"; opacity: modelData.hasMine
        }
        */
    }
        }

    states: State {
        name: "back"; when: modelData.flipped
        PropertyChanges { target: flipable2; angle: 180 }
    }

    transitions: Transition {
        RotationAnimation { duration: 250; direction: flipable2.state == "back" ? RotationAnimation.Counterclockwise : RotationAnimation.Clockwise }
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if (mouse.button == undefined || mouse.button == Qt.RightButton) {
                suspectExclude(index);
            } else {
                if (!modelData.excluded) {
                    if (!solveMode) {
                        suspectFlip(index);
                    } else {
                        guessMurderer(index);
                    }
                } else {
                    suspectExclude(index);
                }
            }
        }
        onPressAndHold: {
            suspectExclude(index);
        }
    }
}
