import QtQuick 1.1

Rectangle {

    property double ratio: 1

    anchors.fill: evidenceTiles
    color: "#000000"

    radius: 15*ratio
    border.width: 3*ratio
    border.color: "#666666"

    Rectangle {

        id: evidenceInfoTitle
        width: parent.width
        height: 40*ratio
        color: evidenceInfo.color
        opacity: 1

        radius: 15*ratio

        border.width: 3*ratio
        border.color: "#666666"
        anchors.top: parent.top

        Text {
            id: evidenceInfoTitleText
            font.capitalization: Font.AllUppercase
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.fill: parent
            font.pixelSize: 24*ratio
            font.weight: Font.Bold
            color: "#ffffff"
            style: Text.Outline
            text: evidenceInfo.title
        }

    }

    Image {
        id: evidenceInfoPicture
        anchors.top: evidenceInfoTitle.bottom
        anchors.topMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.right: parent.right
        anchors.rightMargin: 15
        visible: evidenceInfo.image=="" ? 0 : 1
        source: "pics/"+evidenceInfo.image;
    }

    Text {
        id: evidenceInfoDescriptionText
        wrapMode:Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        anchors.top: evidenceInfoPicture.bottom
        anchors.topMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.right: parent.right
        anchors.rightMargin: 15
        anchors.bottom:parent.bottom
        font.pixelSize: 24*ratio
        font.weight: Font.Bold
        color: "#ffffff"
        style: Text.Outline
        text: evidenceInfo.description

    }

    MouseArea {
        anchors.fill: parent;
        onClicked: {
            if (isPlaying) {
                evidenceInfo.setVisible(false);
            }
        }
    }

}

