// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Column {

    anchors.bottom: suspectlist.bottom
    anchors.right: suspectlist.right
    anchors.bottomMargin: 10
    spacing: 5

    Text {

        id: scoreholder
        anchors.right: parent.right
        horizontalAlignment: Text.AlignRight
        font.pixelSize: 22
        font.weight: Font.Bold
        color: "#ffffff"
        style: Text.Outline
        text: qsTr("__SCORE__")+": " + currentScore + emptyString
    }

    Text {
        id: timeholder
        anchors.right: parent.right
        horizontalAlignment: Text.AlignRight
        font.pixelSize: 22
        font.weight: Font.Bold
        color: "#ffffff"
        style: Text.Outline
        text: qsTr("__TIME__")+": " + timeRemaining + emptyString
    }

    Item {
        id: scoreboardSpacer
        height: 5
        width: 98
    }

    Text {
        anchors.right: parent.right
        horizontalAlignment: Text.AlignRight
        id: evidencecounterholdertext
        font.pixelSize: 22
        font.weight: Font.Bold
        color: "#ffffff"
        style: Text.Outline
        text: qsTr("__FOUND__") + emptyString
    }

    Text {
        anchors.right: parent.right
        horizontalAlignment: Text.AlignRight
        id: evidencecounterholder
        font.pixelSize: 22
        font.weight: Font.Bold
        color: "#ffffff"
        style: Text.Outline
        text: foundEvidences + " / " +  numEvidences
    }

}

