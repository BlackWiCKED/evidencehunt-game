import QtQuick 1.1

ListView {

    anchors.topMargin: 20
    anchors.leftMargin: 20

    width: 180; height: 410

    delegate: Row {
        spacing: 10

        Image {
            source: modelData.position == 1 ? "medals/gold.png" : modelData.position == 2 ? "medals/silver.png" : "medals/bronze.png"
            visible: modelData.position<=3
            smooth: true
            width: 50
            height: 50
        }

        Rectangle {
            color: "transparent"
            visible: modelData.position>3
            width: 50
            height: 50
        }

        Text { font.bold: true; anchors.verticalCenter: parent.verticalCenter; color: modelData.position <=3 ? scoreboardBackground.colors[modelData.position-1] : "#aaaaaa"; font.pixelSize: 24; text: "#" + modelData.position }
        Text { font.bold: true; anchors.verticalCenter: parent.verticalCenter; color: modelData.position <=3 ? scoreboardBackground.colors[modelData.position-1] : "#aaaaaa"; font.pixelSize: 24; text: modelData.name }
        Text { anchors.verticalCenter: parent.verticalCenter; color: modelData.position <=3 ? scoreboardBackground.colors[modelData.position-1] : "#aaaaaa"; font.pixelSize: 24; text: modelData.score }
    }
}
