// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Row {

    property double ratio: 1

    anchors.horizontalCenter: suspectlist.horizontalCenter

    anchors.top: suspectlist.bottom
    anchors.topMargin: 16
    spacing: 85
    scale:ratio
    Image {

        source: "icons/list_num.png"
        MouseArea {
            id: scoreboardButton
            anchors.fill: parent
            onClicked: {
                openScoreboard();
                scoreboardSheet.open();
            }
        }
        Text {
            font.pixelSize: 19
            font.weight: Font.Bold
            color: "#ffffff"
            style: Text.Outline
            text: qsTr("__TOPLIST__") + emptyString
            anchors.top: parent.bottom
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.topMargin: 12
            width:parent.width-3
            horizontalAlignment: Text.AlignHCenter

        }
    }

    Image {
        source: hasWon ? "icons/doc_export.png" : "icons/playback_reload.png"
        MouseArea {
            id: resetButton
            anchors.fill: parent
            onClicked: {
                if (isPlaying) {
                    resetDialog.open();
                } else {
                    reset();
                }
            }
        }
        Text {
            font.pixelSize: 19
            font.weight: Font.Bold
            color: "#ffffff"
            style: Text.Outline
            text: hasWon ? (qsTr("__NEXTCASE__") + emptyString) : (qsTr("__RESTART__") + emptyString)
            anchors.top: parent.bottom
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.topMargin: 12
            width:parent.width-3
            horizontalAlignment: Text.AlignHCenter

        }
    }

    Image {
        opacity: isPlaying ? 1 : 0
        source: solveMode ? "icons/cancel.png" : "icons/lightbulb.png"
        MouseArea {
            id: toggleSolveButton
            anchors.fill: parent
            onClicked: {
                toggleSolve();
            }
        }
        Text {
            font.pixelSize: 19
            font.weight: Font.Bold
            color: "#ffffff"
            style: Text.Outline
            text: (solveMode ? qsTr("__CANCEL__") : qsTr("__SOLVE__")) + emptyString
            anchors.top: parent.bottom
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.topMargin: 12
            width:parent.width-3
            horizontalAlignment: Text.AlignHCenter

        }
    }
}
