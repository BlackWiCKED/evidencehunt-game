import QtQuick 1.1
import Qt.labs.components.native 1.0

import "../DesktopComponents"

Item {

    Image {

        id: scoreboardBackground
        source: "pics/scoreboard.jpg"
        anchors.bottom: parent.bottom
        width: parent.width
        height: parent.height
        smooth: true

        property variant colors: ["#FFB90F", "#CaCaCa", "#A67D3D"]

        Text {
            id: scoreboardToplistHeader
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: 20
            anchors.leftMargin: 20
            text: qsTr("__TOPALL__") + emptyString;
            color: "white"
            font.pixelSize: 24;
            visible: false
        }

        ScoreboardList {
            id: scoreboardToplist
            model: players
            anchors.left: parent.left
            anchors.top: scoreboardToplistHeader.bottom
        }

        Text {
            id: scoreboardToplistMonthHeader
            anchors.top: parent.top
            anchors.left: scoreboardToplist.right
            anchors.topMargin: 20
            anchors.leftMargin: 120
            text: qsTr("__TOPMONTH__") + emptyString;
            color: "white"
            font.pixelSize: 24;
            visible: false
        }

        ScoreboardList {
            id: scoreboardToplistMonth
            model: playersMonth
            anchors.left: scoreboardToplist.right
            anchors.top: scoreboardToplistMonthHeader.bottom
            anchors.leftMargin: 100
        }

        Text {
            id: scoreboardToplistWeekHeader
            anchors.top: parent.top
            anchors.left: scoreboardToplistMonth.right
            anchors.topMargin: 20
            anchors.leftMargin: 120
            text: qsTr("__TOPWEEK__") + emptyString;
            color: "white"
            font.pixelSize: 24;
            visible: false
        }

        ScoreboardList {
            id: scoreboardToplistWeek
            model: playersWeek
            anchors.left: scoreboardToplistMonth.right
            anchors.top: scoreboardToplistWeekHeader.bottom
            anchors.leftMargin: 100
        }

        Text {
            id: scoreboardToplistStatus
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: 20
            anchors.leftMargin: 20
            text: qsTr("__LOADING__") + emptyString;
            color: "white"
            font.pixelSize: 24;
        }

        Button {
            id: retryLoadScoreboardButton
            text: qsTr("__RETRY__") + emptyString
            anchors.top: scoreboardToplistStatus.bottom
            anchors.left: scoreboardToplistStatus.left
            anchors.topMargin: 20
            visible: false
            onClicked: {
                retryLoadBoard();
            }
        }

        Item {
            id: submitScoreboardSection
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.topMargin: 20
            anchors.rightMargin: 20
            Column {

                anchors.right: parent.right
                spacing: 5
                Text {
                    visible: isPlaying || hasWon
                    anchors.right: parent.right
                    color: "white"
                    text: qsTr("__LASTSCORE__") + emptyString + ': ' + currentScore; //'Current score: '
                    font.pixelSize: 28;
                }
                Text {
                    visible: !isPlaying && !hasWon
                    anchors.right: parent.right
                    color: "white"
                    text: qsTr("__LASTSCORE__") + emptyString + ': ' + lastScore; //'Last score: '
                    font.pixelSize: 28;
                }
                Text {
                    anchors.right: parent.right
                    color: "white"
                    text: qsTr("__BESTSCORE__") + emptyString + ': ' + (maxScore > currentScore ? maxScore : currentScore); //'Best score: '
                    font.pixelSize: 28;
                }
                Text {
                    visible: streak > 0
                    anchors.right: parent.right
                    color: "white"
                    text: qsTr("__WINNINGSTREAK__") + emptyString + ": " +streak; //'Winning streak: ' + streak;
                    font.pixelSize: 28;
                }
                Text {
                    visible: isPlaying || hasWon
                    anchors.right: parent.right
                    color: "white"
                    text: qsTr("__FINISHFIRST__") + emptyString; //'Finish the current game first!'
                    font.pixelSize: 28;
                }
                Text {
                    visible: (!isPlaying && !hasWon) && (maxScore <= maxScoreSubmitted)
                    anchors.right: parent.right
                    color: "white"
                    text: qsTr("__WASSUBMITTED__") + emptyString; //'Your best score was submitted!'
                    font.pixelSize: 28;
                }
                Rectangle {
                    visible: maxScore > maxScoreSubmitted && !isPlaying && !hasWon
                    color: "white"
                    border.color: "black"
                    border.width: 3
                    width: parent.width
                    height:31
                    anchors.right: parent.right
                    TextInput {
                        id: playerNameInput
                        anchors.fill: parent
                        anchors.margins: 5
                        font.pixelSize: 18
                        maximumLength: 16
                        anchors.verticalCenter: parent.verticalCenter
                        text: playerName
                        onAccepted: {
                            closeSoftwareInputPanel();
                            focus = false;
                        }
                    }
                }

                Button {
                    id: submitScoreButton
                    anchors.right: parent.right
                    visible: maxScore > maxScoreSubmitted && !isPlaying && !hasWon
                    text: qsTr("__SUBMIT__") + emptyString
                    onClicked: {
                        playerNameInput.closeSoftwareInputPanel();
                        playerNameInput.focus = false;
                        submitScore(playerNameInput.text, maxScore);
                    }
                }

                //TODO: Reset score
                /*
                Button {
                    id: resetScoreButton
                    anchors.right: parent.right
                    text: qsTr("__RESETSCORE__") + emptyString
                    onClicked: {
                        submitScore(playerNameInput.text, maxScore);
                    }
                }
                */

            }
        }

        Button {
            id: onlineScoreboardButton
            text: qsTr("__ONLINE__") + emptyString
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.bottomMargin: 20
            anchors.leftMargin: 20
            font.pixelSize: 32
            onClicked: {
                Qt.openUrlExternally("http://evidencehunt.blackwicked.com");
            }
        }

        state:  (field.width < field.height) ? 'collapsed-portrait':'collapsed-landscape';

        states: [
            State {
                name: "collapsed-portrait";
                StateChangeScript { script: refreshScoreboards(false); }
            },
            State {
                name: "collapsed-landscape";
                StateChangeScript { script: refreshScoreboards(true); }
            }
        ]

    }

    function refreshScoreboards(isVisible) {

        scoreboardToplistMonth.visible = isVisible;
        scoreboardToplistWeek.visible = isVisible;
        scoreboardToplistMonthHeader.visible = isVisible;
        scoreboardToplistWeekHeader.visible = isVisible;

    }

    function toggleScoreboard(isVisible) {

        scoreboardToplist.visible = isVisible;
        scoreboardToplistHeader.visible = isVisible;

        if (scoreboardBackground.state=="collapsed-portrait") {
            isVisible = false;
        }

        scoreboardToplistMonth.visible = isVisible;
        scoreboardToplistWeek.visible = isVisible;
        scoreboardToplistMonthHeader.visible = isVisible;
        scoreboardToplistWeekHeader.visible = isVisible;

    }

    function retryLoadBoard() {

        toggleScoreboard(false);

        scoreboardToplistStatus.visible = true;
        scoreboardToplistStatus.text = qsTr("__LOADING__") + emptyString;
        retryLoadScoreboardButton.visible = false;
        openScoreboard();
    }

    function hasNoError() {

        toggleScoreboard(true);

        scoreboardToplistStatus.visible = false;
        retryLoadScoreboardButton = false;
    }

    function hasError() {

        toggleScoreboard(false);

        scoreboardToplistStatus.visible = true;
        scoreboardToplistStatus.text = qsTr("__CONNECTIONERROR__") + emptyString;
        retryLoadScoreboardButton.visible = true;
    }

}
