import QtQuick 1.1

Flipable {

    id: flipable
    property int angle: 0
    property double ratio: 1
    width: 77*ratio;  height: 77*ratio
    transform: Rotation { origin.x: 38.5*ratio; origin.y: 38.5*ratio; axis.x: 0; axis.y: 1; axis.z: 0; angle: flipable.angle }
    opacity: 0.8

    front: Image {

        scale: ratio

        anchors.centerIn: parent
        source: "tiles/white" + ((((index%2) + Math.floor(index/8)%2)%2 + 1) + (Math.floor(index/8)==7 ? 2 : 0 ) + (Math.floor(index/8)==0 ? 4 : 0 ) + (Math.floor(index%8)==0 ? 8 : 0 ) + (Math.floor(index%8)==7 ? 16 : 0 )) + ".png";
        width: 125; height: 125

        Image {
            scale: ratio
            anchors.centerIn: parent
            source: "icons/delete.png"; opacity: modelData.hasFlag
            width:22
            height:22
            Behavior on opacity { NumberAnimation {} }
        }
        Explosion {
            scale: ratio
            property bool repeatable: true;
            id: mark
            anchors.top: parent.top
            anchors.topMargin: 26
            anchors.left: parent.left
            anchors.leftMargin: 26
        }
    }

    back:  Image {

        scale: ratio

        anchors.centerIn: parent
        source: "tiles/"+(modelData.type.color)+((((index%2) + Math.floor(index/8)%2)%2 + 1) + (Math.floor(index/8)==7 ? 2 : 0 ) + (Math.floor(index/8)==0 ? 4 : 0 ) + (Math.floor(index%8)==0 ? 8 : 0 ) + (Math.floor(index%8)==7 ? 16 : 0 ))+".png";
        width: 125; height: 125

        Text {
            anchors.centerIn: parent
            text: modelData.hint; color: "white"; font.bold: true
            style: Text.Outline
            font.pixelSize: 22
            opacity: modelData.hint > 0
        }

        /*
        Explosion {
            scale:ratio
            id: expl
            anchors.top: parent.top
            anchors.topMargin: 26
            anchors.left: parent.left
            anchors.leftMargin: 26
        }
        */
    }

    states: State {
        name: "back"; when: modelData.flipped
        PropertyChanges { target: flipable; angle: 180 }
    }

    property real pauseDur: 250
    transitions: Transition {
        SequentialAnimation {
            ScriptAction {
                script: {
                    var ret = Math.abs(flipable.x - field.clickx) + Math.abs(flipable.y - field.clicky);
                    if (modelData.hasMine && modelData.flipped) {
                        pauseDur = ret * 3;
                    } else {
                        pauseDur = ret;
                    }
                }
            }
            PauseAnimation {
                duration: pauseDur
            }
            RotationAnimation { easing.type: Easing.InOutQuad }
            //ScriptAction { script: if (modelData.hasEvidence && modelData.flipped) { expl.explode = true } }
        }
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            var row = Math.floor(index / 8);
            var col = index - (Math.floor(index / 8) * 8);
            if (mouse.button == undefined || mouse.button == Qt.RightButton) {
                flag(row, col);
                mark.callExplode();
            } else {
                flip(row, col);
                if (modelData.flipped) {
                    if (modelData.hasEvidence) {
                        loadEvidence(row, col);
                        //expl.explode = true;
                    }
                }
            }
        }
        onPressAndHold: {
            var row = Math.floor(index / 8);
            var col = index - (Math.floor(index / 8) * 8);
            flag(row, col);
            mark.callExplode();
        }
    }
}
