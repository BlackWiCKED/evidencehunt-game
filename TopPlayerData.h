#include <QObject>

#ifndef TOPPLAYERDATA_H
#define TOPPLAYERDATA_H

class TopPlayerData : public QObject {

    Q_OBJECT

public:

    TopPlayerData(QString name="", QString score="", QString position="") : _name(""), _score(""), _position("") {
        _name = name;
        _score = score;
        _position = position;
        emit dataChanged();
    }

    Q_PROPERTY(QString name READ name NOTIFY dataChanged())
    QString name() const { return _name; }

    Q_PROPERTY(QString score READ score NOTIFY dataChanged())
    QString score() const { return _score; }

    Q_PROPERTY(QString position READ position NOTIFY dataChanged())
    QString position() const { return _position; }

signals:
    void dataChanged();

private:
    QString _name;
    QString _score;
    QString _position;

};

#endif // TOPPLAYERDATA_H
