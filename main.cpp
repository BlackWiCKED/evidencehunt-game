#include <QDeclarativeEngine>
#include <QDeclarativeContext>
#include <QDeclarativePropertyMap>
#include <QtGui/QApplication>
#include <QIcon>
#include <QSettings>
#include <QTranslator>
#include <QLocale>

#include <QGraphicsObject>

#include "qmlapplicationviewer.h"
#include "EvidenceHuntGame.h"

#include <QDesktopWidget>

#include <QDebug>

Q_DECL_EXPORT int main(int argc, char *argv[]) {

    QApplication app(argc, argv);

    app.setOrganizationName("BlackWiCKED");
    app.setOrganizationDomain("blackwicked.com");
    app.setApplicationName("EvidenceHunt Game");

    app.setApplicationName("EvidenceHunt Game");
    app.setApplicationVersion("1.5.2");
    app.setWindowIcon(QIcon(":/qml/EvidencehuntCore/pics/icon.ico"));

    QmlApplicationViewer canvas;

    qmlRegisterType<TileData>();
    qmlRegisterType<SuspectData>();
    qmlRegisterType<TopPlayerData>();

    qmlRegisterType<EvidenceType>();
    qmlRegisterType<EvidenceInfo>();

    EvidencehuntGame* game = new EvidencehuntGame(&app);
    canvas.engine()->rootContext()->setContextObject(game);

    #if defined(Q_EVHUNT_MAEMO) || defined(Q_EVHUNT_MEEGO)
        canvas.setOrientation(QmlApplicationViewer::ScreenOrientationLockLandscape);
        canvas.setSource(QUrl("qrc:///qml/harmattan.qml"));
        canvas.setGeometry(QRect(0, 0, 854, 480));
        canvas.showFullScreen();
        qDebug() << "Harmattan";
    #elif defined(Q_EVHUNT_SYMBIAN)
        canvas.setSource(QUrl("qrc:///qml/harmattan.qml"));
        canvas.setGeometry(QRect(0, 0, 854, 480));
        canvas.showFullScreen();
        qDebug() << "Symbian";
    #elif defined(Q_EVHUNT_BLACKBERRY)



        QDeclarativePropertyMap* displayProperties = new QDeclarativePropertyMap;
        displayProperties->insert("width", QVariant(768));
        displayProperties->insert("height", QVariant(1280));

        canvas.engine()->rootContext()->setContextProperty("DisplayInfo", displayProperties);

        canvas.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
        canvas.setSource(QUrl("qrc:///qml/blackberry.qml"));
        canvas.setGeometry(QRect(0, 0, 1280, 768));
        canvas.showFullScreen();
        qDebug() << "Blackberry";
    #else

        canvas.setSource(QUrl("qrc:///qml/desktop.qml"));

        int width = 854;
        int height = 482;
        QRect screenGeometry = app.desktop()->screenGeometry();
        int screenWidth = screenGeometry.width();
        int screenHeight = screenGeometry.height();
        QRect geometry = QRect((screenWidth - width)/2, (screenHeight - height)/2, width, height);
        canvas.setGeometry(geometry);

        canvas.setWindowTitle("EvidenceHunt Game - " + app.applicationVersion());
        canvas.setFixedSize(width,height);
        canvas.showExpanded();

        #if defined(Q_EVHUNT_WINDOWS)
            qDebug() << "Windows Desktop";
        #else
            qDebug() << "Linux Desktop";
        #endif

    #endif

    QObject *rootObject = dynamic_cast<QObject*>(canvas.rootObject());
    QObject::connect(canvas.engine(), SIGNAL(quit()), &app, SLOT(quit()));
    QObject::connect(game, SIGNAL(openWelcomeDialog()), rootObject, SLOT(openWelcomeDialog()));
    QObject::connect(game, SIGNAL(scoreboardLoaded()), rootObject, SLOT(scoreBoardLoaded()));
    QObject::connect(game, SIGNAL(openBoardErrorDialog()), rootObject, SLOT(openBoardErrorDialog()));

    return app.exec();

 }
