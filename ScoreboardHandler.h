#include <QtXml/QXmlDefaultHandler>
#include <QDebug>

#ifndef SCOREBOARDHANDLER_H
#define SCOREBOARDHANDLER_H

class Handler : public QXmlDefaultHandler {

public:

  Handler() {
    data = new QList< QHash<QString,QString> >;
  }

  QList< QHash<QString,QString> > getData() {
      return *data;
  }

  bool startDocument() {
    inScoreboard = false;
    return true;
  }

  bool endElement( const QString&, const QString&, const QString &name ) {
    if( name == "scoreboard" ) inScoreboard = false;
    return true;
  }

  bool startElement( const QString&, const QString&, const QString &name, const QXmlAttributes &attrs ) {
    QHash<QString,QString> thisData;
    if( inScoreboard && name=="player" ) {
        for( int i=0; i<attrs.count(); i++ ) {
           thisData[attrs.localName(i)] = attrs.value(i);
        }
        data->append(thisData);

    } else if( name == "scoreboard" ) {
        inScoreboard = true;
    }

    return true;
  }

  private:
    bool inScoreboard;
    QList< QHash<QString,QString> > *data;


};

#endif // SCOREBOARDHANDLER_H
