#include <QObject>

#include "TileData.h"

#ifndef EVIDENCEINFO_H
#define EVIDENCEINFO_H

class EvidenceInfo : public QObject
{
    Q_OBJECT

public:

    EvidenceInfo() : _title(""), _type(new EvidenceType), _description(""), _image(""), _isVisible(false) {}

    Q_PROPERTY(QString title READ title NOTIFY evidenceChanged)
    QString title() const { return _title; }

    Q_PROPERTY(QString color READ color NOTIFY evidenceChanged)
    QString color() const { return _type->colorcode(); }

    Q_PROPERTY(QString description READ description NOTIFY evidenceChanged)
    QString description() const { return _description; }

    Q_PROPERTY(QString image READ image NOTIFY evidenceChanged)
    QString image() const { return _image == "" ? "empty.png" : _image; }

    Q_PROPERTY(bool visible READ visible NOTIFY evidenceChanged)
    bool visible() const { return _isVisible; }

    void loadEvidence(TileData *t);
    void loadCustomMessage(QString, EvidenceType*, QString, QString);

public slots:
    Q_INVOKABLE void setVisible(bool visible) { _isVisible = visible; emit evidenceChanged(); }

signals:
    void evidenceChanged();

private:
    QString _title;
    EvidenceType* _type;
    QString _description;
    QString _image;
    bool _isVisible;
};

#endif // EVIDENCEINFO_H
